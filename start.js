const mongoose = require('mongoose');

// Make sure we are running node 7.6+
const [major, minor] = process.versions.node.split('.').map(parseFloat);
if (major < 7 || (major === 7 && minor <= 5)) {
  console.log('🛑 🌮 🐶 💪 💩\nHey You! \n\t ya you! \n\t\tBuster! \n\tYou\'re on an older version of node that doesn\'t support the latest and greatest things we are learning (Async + Await)! Please go to nodejs.org and download version 7.6 or greater. 👌\n ');
  process.exit();
}

// import environmental variables from our variables.env file
require('dotenv').config({ path: 'variables.env' });

// Connect to our Database and handle any bad connections
mongoose.connect("mongodb://Nathalie:FDREXE0@baloudureventon-shard-00-00.fzv6w.mongodb.net:27017,baloudureventon-shard-00-01.fzv6w.mongodb.net:27017,baloudureventon-shard-00-02.fzv6w.mongodb.net:27017/BalouDuReventon?ssl=true&replicaSet=atlas-cmdthl-shard-0&authSource=admin&retryWrites=false&w=majority");
mongoose.Promise = global.Promise; // Tell Mongoose to use ES6 promises
mongoose.connection.on('error', (err) => {
  console.error(`🙅 🚫 🙅 🚫 🙅 🚫 🙅 🚫 → ${err.message}`);
});

// READY?! Let's go!
require('./models/Stallion');
require('./models/Horse');
require('./models/Vet');
require('./models/User');
require('./models/Event');
require('./models/Article');
require('./models/Home');
require('./models/Review');
require('./models/Commentaar');
require('./models/CommentaarVet');
require('./models/CommentaarEvent');
require('./models/Order');
require('./models/Mare');

// Start our app!
const app = require('./app');
app.set('port', process.env.PORT || 3000);
const server = app.listen(app.get('port'), () => {
  console.log(`Express running → PORT ${server.address().port}`);
});

// temp
require('./handlers/mail');