const express = require('express');
const router = express.Router({ mergeParams: true });
const stallionController = require('../controllers/stallionController');
const horseController = require('../controllers/horseController');
const vetController = require('../controllers/vetController');
const userController = require('../controllers/userController');
const eventController = require('../controllers/eventController');
const articleController = require('../controllers/articleController');
const authController = require('../controllers/authController');
const reviewController = require('../controllers/reviewController');
const homeController = require('../controllers/homeController');
const commentaarController = require('../controllers/commentaarController');
const commentaarVetController = require('../controllers/commentaarVetController');
const commentaarEventController = require('../controllers/commentaarEventController');
const orderController = require('../controllers/orderController');
const mareController = require('../controllers/mareController');
const { catchErrors } = require('../handlers/errorHandlers');

// set locale based on route
const setLocale = function (req, res, next) {
    if (req.params && req.params.locale) {
        req.setLocale(req.params.locale);
    }
    next();
}

// Do work here
router.get(`/home`, setLocale, 
    catchErrors(homeController.homePage));
router.get(`/admin`, setLocale, authController.isLoggedIn, 
    catchErrors(userController.getAdmin));
router.get('/stallions', setLocale, catchErrors(horseController.getStallions));
router.get('/allhorses', setLocale, catchErrors(horseController.getAllhorses));
router.get('/sporthorses', setLocale, catchErrors(horseController.getSporthorses));
router.get('/sporthorses/level', setLocale, catchErrors(horseController.getSporthorsesByLevel));
router.get('/sporthorses/levelZ', setLocale, catchErrors(horseController.getSporthorsesByLevelZ));
router.get('/sporthorses/name', setLocale, catchErrors(horseController.getSporthorsesByName));
router.get('/sporthorses/nameZ', setLocale, catchErrors(horseController.getSporthorsesByNameZ));
router.get('/sporthorses/age', setLocale, catchErrors(horseController.getSporthorsesByAge));
router.get('/sporthorses/ageZ', setLocale, catchErrors(horseController.getSporthorsesByAgeZ));
router.get('/broodmares', setLocale, catchErrors(horseController.getFokmerries));
router.get('/broodmares/level', setLocale, catchErrors(horseController.getBroodmaresByLevel));
router.get('/broodmares/levelZ', setLocale, catchErrors(horseController.getBroodmaresByLevelZ));
router.get('/broodmares/name', setLocale, catchErrors(horseController.getBroodmaresByName));
router.get('/broodmares/nameZ', setLocale, catchErrors(horseController.getBroodmaresByNameZ));
router.get('/broodmares/age', setLocale, catchErrors(horseController.getBroodmaresByAge));
router.get('/broodmares/ageZ', setLocale, catchErrors(horseController.getBroodmaresByAgeZ));
router.get('/younghorses', setLocale, catchErrors(horseController.getYoungHorses));
router.get('/younghorses/level', setLocale, catchErrors(horseController.getYoungHorsesByLevel));
router.get('/younghorses/levelZ', setLocale, catchErrors(horseController.getYoungHorsesByLevelZ));
router.get('/younghorses/name', setLocale, catchErrors(horseController.getYoungHorsesByName));
router.get('/younghorses/nameZ', setLocale, catchErrors(horseController.getYoungHorsesByNameZ));
router.get('/younghorses/age', setLocale, catchErrors(horseController.getYoungHorsesByAge));
router.get('/younghorses/ageZ', setLocale, catchErrors(horseController.getYoungHorsesByAgeZ));
router.get('/foals', setLocale, catchErrors(horseController.getFoals));
router.get('/foals-dressage', setLocale, catchErrors(horseController.getDressageFoals));
router.get('/dressage', setLocale, catchErrors(horseController.getDressageHorses));
router.get('/broodmares-dressage', setLocale, catchErrors(horseController.getDressageBroodmares));
router.get('/younghorses-dressage', setLocale, catchErrors(horseController.getDressageYoungHorses));
router.get('/sporthorses-dressage', setLocale, catchErrors(horseController.getDressageSporthorses));
router.get('/foals/sire', setLocale, catchErrors(horseController.getFoalsBySire));
router.get('/foals/sireZ', setLocale, catchErrors(horseController.getFoalsBySireZ));
router.get('/foals/name', setLocale, catchErrors(horseController.getFoalsByName));
router.get('/foals/nameZ', setLocale, catchErrors(horseController.getFoalsByNameZ));
router.get('/foals/dam', setLocale, catchErrors(horseController.getFoalsByDam));
router.get('/foals/damZ', setLocale, catchErrors(horseController.getFoalsByDamZ));
router.get('/dressagefoals/sire', setLocale, catchErrors(horseController.getDressageFoalsBySire));
router.get('/dressagefoals/sireZ', setLocale, catchErrors(horseController.getDressageFoalsBySireZ));
router.get('/dressagefoals/name', setLocale, catchErrors(horseController.getDressageFoalsByName));
router.get('/dressagefoals/nameZ', setLocale, catchErrors(horseController.getDressageFoalsByNameZ));
router.get('/dressagefoals/dam', setLocale, catchErrors(horseController.getDressageFoalsByDam));
router.get('/dressagefoals/damZ', setLocale, catchErrors(horseController.getDressageFoalsByDamZ));
router.get('/forsale', setLocale, catchErrors(horseController.getForSale));
router.get('/forsale/level', setLocale, catchErrors(horseController.getForSaleByLevel));
router.get('/forsale/levelZ', setLocale, catchErrors(horseController.getForSaleByLevelZ));
router.get('/forsale/name', setLocale, catchErrors(horseController.getForSaleByName));
router.get('/forsale/nameZ', setLocale, catchErrors(horseController.getForSaleByNameZ));
router.get('/forsale/age', setLocale, catchErrors(horseController.getForSaleByAge));
router.get('/forsale/ageZ', setLocale, catchErrors(horseController.getForSaleByAgeZ));
router.get('/breedingprogram', setLocale, catchErrors(horseController.getBreedingProgram));
router.get('/stabling', setLocale, catchErrors(horseController.getStabling));
router.get('/rearing', setLocale, catchErrors(horseController.getRearing));
router.get('/opfok', setLocale, catchErrors(horseController.getOpfok));
router.get('/embryos', setLocale, catchErrors(horseController.getEmbryos));
router.get('/embryos/sire', setLocale, catchErrors(horseController.getEmbryosBySire));
router.get('/embryos/sireZ', setLocale, catchErrors(horseController.getEmbryosBySireZ));
router.get('/embryos/name', setLocale, catchErrors(horseController.getEmbryosByName));
router.get('/embryos/nameZ', setLocale, catchErrors(horseController.getEmbryosByNameZ));
router.get('/embryos/dam', setLocale, catchErrors(horseController.getEmbryosByDam));
router.get('/embryos/damZ', setLocale, catchErrors(horseController.getEmbryosByDamZ));
router.get('/yearlings', setLocale, catchErrors(horseController.getYearlings));
router.get('/yearlings/sire', setLocale, catchErrors(horseController.getYearlingsBySire));
router.get('/yearlings/sireZ', setLocale, catchErrors(horseController.getYearlingsBySireZ));
router.get('/yearlings/name', setLocale, catchErrors(horseController.getYearlingsByName));
router.get('/yearlings/nameZ', setLocale, catchErrors(horseController.getYearlingsByNameZ));
router.get('/yearlings/dam', setLocale, catchErrors(horseController.getYearlingsByDam));
router.get('/yearlings/damZ', setLocale, catchErrors(horseController.getYearlingsByDamZ));
router.get('/2-year-olds', setLocale, catchErrors(horseController.get2yolds));
router.get('/2-year-olds/sire', setLocale, catchErrors(horseController.get2yoldsBySire));
router.get('/2-year-olds/sireZ', setLocale, catchErrors(horseController.get2yoldsBySireZ));
router.get('/2-year-olds/name', setLocale, catchErrors(horseController.get2yoldsByName));
router.get('/2-year-olds/nameZ', setLocale, catchErrors(horseController.get2yoldsByNameZ));
router.get('/2-year-olds/dam', setLocale, catchErrors(horseController.get2yoldsByDam));
router.get('/2-year-olds/damZ', setLocale, catchErrors(horseController.get2yoldsByDamZ));
router.get('/3-year-olds', setLocale, catchErrors(horseController.get3yolds));
router.get('/3-year-olds/sire', setLocale, catchErrors(horseController.get3yoldsBySire));
router.get('/3-year-olds/sireZ', setLocale, catchErrors(horseController.get3yoldsBySireZ));
router.get('/3-year-olds/name', setLocale, catchErrors(horseController.get3yoldsByName));
router.get('/3-year-olds/nameZ', setLocale, catchErrors(horseController.get3yoldsByNameZ));
router.get('/3-year-olds/dam', setLocale, catchErrors(horseController.get3yoldsByDam));
router.get('/3-year-olds/damZ', setLocale, catchErrors(horseController.get3yoldsByDamZ));
router.get('/4-year-olds', setLocale, catchErrors(horseController.get4yolds));
router.get('/4-year-olds/sire', setLocale, catchErrors(horseController.get4yoldsBySire));
router.get('/4-year-olds/sireZ', setLocale, catchErrors(horseController.get4yoldsBySireZ));
router.get('/4-year-olds/name', setLocale, catchErrors(horseController.get4yoldsByName));
router.get('/4-year-olds/nameZ', setLocale, catchErrors(horseController.get4yoldsByNameZ));
router.get('/4-year-olds/dam', setLocale, catchErrors(horseController.get4yoldsByDam));
router.get('/4-year-olds/damZ', setLocale, catchErrors(horseController.get4yoldsByDamZ));
router.get('/embryos/page/:page', setLocale, catchErrors(horseController.getEmbryos));
router.get('/references', setLocale, catchErrors(horseController.getReferences));
router.get('/references/level', setLocale, catchErrors(horseController.getReferencesByLevel));
router.get('/references/levelZ', setLocale, catchErrors(horseController.getReferencesByLevelZ));
router.get('/references/name', setLocale, catchErrors(horseController.getReferencesByName));
router.get('/references/nameZ', setLocale, catchErrors(horseController.getReferencesByNameZ));
router.get('/references/age', setLocale, catchErrors(horseController.getReferencesByAge));
router.get('/references/ageZ', setLocale, catchErrors(horseController.getReferencesByAgeZ));
router.get('/news', setLocale, catchErrors(articleController.getArticlesByTag));
router.get('/allArticles', setLocale, catchErrors(articleController.getArticlesByTitle));
router.get('/stallions/search', catchErrors(stallionController.searchStallions));
router.get('/stallions/page/:page', catchErrors(stallionController.getStallions));
router.get('/contact', horseController.contactPage)
router.get('/orderSemen/:id', orderController.orderSemen);
//router.get('/centers', catchErrors(vetController.getVets));
//router.get('/calendar', catchErrors(eventController.getEvents));
//router.get('/news', catchErrors(articleController.getArticles));

router.get('/addStallion', setLocale, authController.isLoggedIn, stallionController.addStallion);
router.get('/addHorse', authController.isLoggedIn, setLocale, horseController.addHorse);
router.get('/addMare',setLocale,  authController.isLoggedIn, mareController.addMare);
router.get('/addVet', setLocale, authController.isLoggedIn, vetController.addVet);
router.get('/addEvent', setLocale, authController.isLoggedIn, eventController.addEvent);
router.get('/addArticle', setLocale, authController.isLoggedIn, articleController.addArticle);
router.get('/addHome', setLocale, authController.isLoggedIn, homeController.addHome);

router.post('/addStallion', 
    stallionController.upload, 
    catchErrors(stallionController.resize), 
    catchErrors(stallionController.createStallion));
router.post('/addMare', 
    mareController.upload, 
    catchErrors(mareController.resize), 
    catchErrors(mareController.createMare));
router.post('/addHorse', 
    horseController.upload, 
    catchErrors(horseController.resize), 
    catchErrors(horseController.createHorse));

router.post('/addOrder/:id', 
    orderController.upload, 
    catchErrors(orderController.resize), 
    catchErrors(orderController.createOrder));

router.post('/addVet', 
    vetController.upload, 
    catchErrors(vetController.resize), 
    catchErrors(vetController.createVet));

router.post('/addEvent', 
    eventController.upload, 
    catchErrors(eventController.resize), 
    catchErrors(eventController.createEvent));

router.post('/addArticle', 
    articleController.upload, 
    catchErrors(articleController.resize), 
    catchErrors(articleController.createArticle));
    
router.post('/editHome', 
    homeController.upload, 
    catchErrors(homeController.resize), 
    catchErrors(homeController.createHome));

router.post('/addStallion/:id', 
    stallionController.upload, 
    catchErrors(stallionController.resize),
    catchErrors(stallionController.updateStallion));

    router.post('/addHorse/:id', 
    horseController.upload, 
    catchErrors(horseController.resize),
    catchErrors(horseController.updateHorse));

router.post('/addMare/:id', 
    mareController.upload, 
    catchErrors(mareController.resize),
    catchErrors(mareController.updateMare));

router.post('/addHorse/:id', 
    horseController.upload, 
    catchErrors(horseController.resize),
    catchErrors(horseController.updateHorse));

router.post('/addVet/:id', 
    vetController.upload, 
    catchErrors(vetController.resize),
    catchErrors(vetController.updateVet));

router.post('/addEvent/:id', 
    eventController.upload, 
    catchErrors(eventController.resize),
    catchErrors(eventController.updateEvent));

router.post('/addArticle/:id', 
    articleController.upload, 
    catchErrors(articleController.resize),
    catchErrors(articleController.updateArticle));

router.post('/editHome/:id', 
    homeController.upload, 
    catchErrors(homeController.resize),
    catchErrors(homeController.updateHome));

router.get('/stallions/:id/edit', catchErrors(stallionController.editStallion));
router.get('/horses/:id/edit', catchErrors(horseController.editHorse));
router.get('/mares/:id/edit', catchErrors(mareController.editMare));
router.get('/centers/:id/edit', catchErrors(vetController.editVet));
router.get('/calendar/:id/edit', catchErrors(eventController.editEvent));
router.get('/news/:id/edit', catchErrors(articleController.editArticle));
router.get('/home/:id/edit', catchErrors(homeController.editHome));

router.get('/stallion/:slug', setLocale, catchErrors(stallionController.getStallionBySlug));
router.get('/horse/:slug', setLocale, catchErrors(horseController.getHorseBySlug));
router.get('/mare/:slug', catchErrors(mareController.getMareBySlug));
router.get('/centers/:slug', catchErrors(vetController.getVetBySlug));
router.get('/calendar/:slug', catchErrors(eventController.getEventBySlug));
router.get('/news/:slug', setLocale, catchErrors(articleController.getArticleBySlug));
router.get('/order/:id', catchErrors(orderController.getOrderById));

router.get('/login', setLocale, userController.loginForm);
router.post('/login', setLocale, authController.login);

router.get('/register', setLocale, userController.registerForm);
router.post('/register',setLocale, 
    userController.validateRegister,
    userController.register,
    authController.login
);

router.get('/logout', authController.logout);
router.get('/error', stallionController.errorPage);

// router.get('/account', authController.isLoggedIn, userController.account);
router.post('/account',
     userController.upload, 
    catchErrors(userController.resize), 
    catchErrors(userController.updateAccount));
router.post('/account/forgot', catchErrors(authController.forgot));
router.get('/account/reset/:token', catchErrors(authController.reset));
router.post('/account/reset/:token', 
authController.confirmedPasswords, 
catchErrors(authController.update));
router.post('/contact', setLocale, catchErrors(authController.sendMail));


router.get('/tags/:tag', catchErrors(articleController.getArticlesByTag));

router.get('/vets', catchErrors(vetController.getVetsByService));
router.get('/vets/:service', catchErrors(vetController.getVetsByService));

router.get('/categories', catchErrors(eventController.getEventsByCategory));
router.get('/categories/:category', catchErrors(eventController.getEventsByCategory));

//router.get('/centers', vetController.mapPage);
router.get('/center', catchErrors(vetController.getVetsByServiceMap));
router.get('/center/:service', catchErrors(vetController.getVetsByServiceMap));

router.get('/account', authController.isLoggedIn, catchErrors(userController.getHearts));
router.post('/reviews/:id', authController.isLoggedIn, catchErrors(reviewController.addReview));
router.post('/comments/:id', authController.isLoggedIn, catchErrors(commentaarController.addComment));
router.post('/commentsVet/:id', authController.isLoggedIn, 
    catchErrors(commentaarVetController.addCommentVet));
router.post('/commentsEvent/:id', authController.isLoggedIn, 
    catchErrors(commentaarEventController.addCommentEvent));
router.post('/orderSemen/:id', catchErrors(orderController.addOrder));


router.get('/top', catchErrors(stallionController.getTopStallions));
/* API */

router.get('/api/searchNews', catchErrors(articleController.searchArticles));
router.get('/api/searchCalendar', catchErrors(eventController.searchEvents));
router.get('/api/searchVets', catchErrors(vetController.searchVets));
router.get('/api/vets/near', catchErrors(vetController.mapVets));
router.post('/api/stallions/:id/heart', catchErrors(stallionController.heartStallion));
router.get('/sires/autocomplete', catchErrors(horseController.siresAutocomplete));
router.get('/dams/autocomplete', catchErrors(horseController.damsAutocomplete));

router.get('/test', catchErrors(articleController.searchTest));
module.exports = router;
