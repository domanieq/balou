const express = require('express');
const router = express.Router();

// redirect to language
router.get('/', (req, res) => {
    const locale = req.getLocale();
    res.redirect(`${locale}/home`);
});

// router.get('/news', (req, res) => {
//     const locale = req.getLocale();
//     res.redirect(`${locale}/news`);
// });

// router.get('/stallions', (req, res) => {
//     const locale = req.getLocale();
//     res.redirect(`${locale}/stallions`);
// });

// router.get('/addStallion', (req, res) => {
//     const locale = req.getLocale();
//     res.redirect(`${locale}/addStallion`);
// });



// router.get('/categories', (req, res) => {
//     const locale = req.getLocale();
//     res.redirect(`${locale}/categories`);
// });

// router.get('/addEvent', (req, res) => {
//     const locale = req.getLocale();
//     res.redirect(`${locale}/addEvent`);
// });

// router.get('/contact', (req, res) => {
//     const locale = req.getLocale();
//     res.redirect(`${locale}/contact`);
// });

// router.get('/account', (req, res) => {
//     const locale = req.getLocale();
//     res.redirect(`${locale}/account`);
// });

// router.get('/login', (req, res) => {
//     const locale = req.getLocale();
//     res.redirect(`${locale}/login`);
// });

// router.get('/register', (req, res) => {
//     const locale = req.getLocale();
//     res.redirect(`${locale}/register`);
// });

// router.get('/mares/:id', (req, res) => {
//     const locale = req.getLocale();
//     res.redirect(`${locale}/mares/:id`);
// });

// router.get('/mares/:slug', (req, res) => {
//     const locale = req.getLocale();
//     res.redirect(`${locale}/mares/:slug`);
// });

// router.get('/logout', (req, res) => {
//     const locale = req.getLocale();
//     res.redirect(`${locale}/logout`);
// });

module.exports = router;
