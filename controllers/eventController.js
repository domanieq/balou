const mongoose = require('mongoose');
const Event = mongoose.model('Event');
const multer = require('multer');
const jimp = require('jimp');
const uuid = require('uuid');

const multerOptions = {
    storage: multer.memoryStorage(),
    fileFilter(req,file,next) {
        const isPhoto = file.mimetype.startsWith('image/');
        if(isPhoto) {
            next(null, true);
        } else {
            next({ message: 'That filetype is not allowed!'})
        }
    }
};

exports.homePage = (req, res) => {
   res.render('calendar', {title: 'Calendar'});  
}

exports.addEvent = (req,res) => {
    res.render('editEvent', {title: 'Add Event'});
};

exports.upload = multer(multerOptions).single("file[]");

exports.resize = async (req, res, next) => {
    if (!req.file) {
        next();
        return;
    }
    const extension = req.file.mimetype.split('/')[1];
    req.body.file = `${uuid.v4()}.${extension}`;
    const file = await jimp.read(req.file.buffer);
    await file.resize(800, jimp.AUTO);
    await file.write(`./public/uploads/${req.body.file}`);
    next();
};

exports.createEvent = async (req,res) => {
    req.body.owner = req.user._id;
    const event = await (new Event(req.body)).save();
    req.flash('success', `Successfully created ${event.name}. Would  you like to take a look?`);
    res.redirect(`/calendar/${event.slug}`);
};

exports.getEvents = async (req, res) => {
    const events = await (Event.find());
    res.render('calendar', { title: 'Calendar', events });
};

const confirmOwner = (event, user) => {
    if (!event.owner.equals(user._id) || user.level > 10) {
        req.flash('error', `You must own an event in order to edit it.`);
        res.redirect(`/calendar`);;
    }
};

exports.editEvent = async (req,res) => {
    const event = await Event.findOne({ _id: req.params.id });
    confirmOwner(event, req.user);
    res.render('editEvent', { title: `Edit ${event.name}`, event})
};

exports.updateEvent = async (req,res) => {
    req.body.location.type = 'Point';
    const event = await Event.findOneAndUpdate({ _id: req.params.id}, req.body, {
        new: true,
        runValidators: true,
    }).exec();
    req.flash('success', `Successfully updated ${event.name}. <a href="/calendar/${event.slug}">View Event</a>`);
    res.redirect(`/calendar/${event._id}/edit`);
};

exports.getEventBySlug = async (req,res) => {
    const event = await Event.findOne({ slug: req.params.slug }).populate('owner');
    if (!event) return next();
    res.render('event', { event, title: event.name });
};

exports.searchEvents = async (req,res) => {
    const events = await Event.find({
        $text: {
            $search: req.query.q,
        
        }
    }, {
            score: { $meta: 'textScore'}
    }).sort({
        score: { $meta: 'textScore'}
    });//.limit(5)
    res.json(events);
};

exports.getEventsByCategory = async (req, res) => {
    const category = req.params.category;
    const categoryQuery = category || { $exists:true};
    const categoriesPromise = Event.getCategorysList();
    const eventsPromise = Event.find({ category: categoryQuery});
    const [categorys, events] = await Promise.all([categoriesPromise, eventsPromise]);
    res.render('categorys', { categorys, title: 'Calendar', category, events });
};