const mongoose = require('mongoose');
const Article = mongoose.model('Article');
const multer = require('multer');
const jimp = require('jimp');
const uuid = require('uuid');

const multerOptions = {
    storage: multer.memoryStorage(),
    fileFilter(req, file, next) {
      const isPhoto = file.mimetype.startsWith('image/');
      if(isPhoto) {
        next(null, true);
      } else {
        next({ message: 'That filetype isn\'t allowed!' }, false);
      }
    }
  };

exports.homePage = (req, res) => {
   res.render('index', {title: 'Index'});  
}

exports.addArticle = (req,res) => {
    res.render('addArticle', {title: 'Add Article'});
};

exports.upload = multer(multerOptions).fields([
    {
        name: 'file', maxCount: 10
    },
    {
        name: 'extra', maxCount: 10
    }
]);

exports.resize = async (req, res, next) => {
    // do not resize if nothing
    if (!req.files) {
        return next();
    }
    // regex to take file extension
    const re = /(?:\.([^.]+))?$/;
    // look by all possible file uploads
    for (let key of ['file', 'fileOffspring']) {
        const files = req.files[key];
        if (Array.isArray(files) && files.length > 0) {
            // set initial empty array
            req.body[key] = [];
            for (let file of files) {
                // take extension and build name
                const extension = re.exec(file.originalname)[1]; 
                const name = `${uuid.v4()}.${(extension || '').toLowerCase()}`;
                // resize buffer
                const resizedFile = await jimp.read(file.buffer);
                await resizedFile.resize(800, jimp.AUTO);
                await resizedFile.write(`./public/uploads/${name}`);
                // add file name to the array
                req.body[key].push(name);
            }
        }
    }

    next();
};

exports.createArticle = async (req,res) => {
    req.body.owner = req.user._id;
    const locale = req.getLocale();
    const article = await (new Article(req.body)).save();
    req.flash('success', `Successfully created ${article.title}. Take a look here!`);
    res.redirect(`/${locale}/news/${article.slug}`);
};

exports.getArticles = async (req, res) => {
    const articles = await (Article
        .find()
        .sort({ updated: -1})
        .limit(2)
    );
    res.locals.path = req.path;
    res.render('articles', { title: 'News', articles,  path: req.path });
};

const confirmOwner = (article, user) => {
    if (!article.owner.equals(user._id) || user.level > 10) {
        req.flash('error', `You must be an editor in order to add news.`);
        res.redirect(`/news`);
    }
};

exports.editArticle = async (req,res) => {
    const article = await Article.findOne({ _id: req.params.id });
    confirmOwner(article, req.user);
    res.render('editArticle', { title: `Edit ${article.title}`, article})
};

exports.updateArticle = async (req,res) => {
    const article = await Article.findOneAndUpdate({ _id: req.params.id}, req.body, {
        new: true,
        runValidators: true,
    }).exec();
    const locale = req.getLocale();
    req.flash('success', `Successfully updated ${article.title}. <a href="/news/${article.slug}">View Article</a>`);
    res.redirect(`/${locale}/news/${article._id}/edit`);
};

exports.getArticleBySlug = async (req,res) => {
    const article = await Article.findOne({ slug: req.params.slug }).populate('owner');
    const last5articles = await (Article
        .find()
        .sort({ updated: -1})
        .limit(5)
    );
    const prevarticle = await Article.findOne().sort({ updated: -1}).limit(1);
    const nextarticle = await Article.findOne().sort({ updated: 1}).limit(1);
    const locale = req.getLocale();
    if (!article) return next();
    res.render('article', { article, title: article.title, last5articles, prevarticle, nextarticle, locale });
};

exports.searchArticles = async (req,res) => {
    const articles = await Article.find({
        $text: {
            $search: req.query.q
        }
    }, {
            score: { $meta: 'textScore'}
    }).sort({
        score: { $meta: 'textScore'}
    });//.limit(5)
    res.json(articles);
};

exports.searchTest = async (req,res) => {
    const articles = await Article.find({
        $text: {
            $search: req.query.q
        }
    }, {
            score: { $meta: 'textScore'}
    }).sort({
        score: { $meta: 'textScore'}
    });//.limit(5)
    res.json(articles);
};

exports.getArticlesByTag = async (req,res) => {
    const tag = req.params.tag;
    const tagQuery = tag || { $exists: true};
    const tagsPromise = Article.getTagsList();
    const locale = req.getLocale();
    res.locals.path = req.path;
    const articlesPromise = Article.find({ tags: tagQuery}).sort({updated: -1});
    const [tags, articles] = await Promise.all([tagsPromise, articlesPromise]);
    res.render('articles', { tags, title: 'News', tag, articles, locale, path: req.path});
};

exports.getArticlesByTitle = async (req,res) => {
    const tag = req.params.tag;
    const tagQuery = tag || { $exists: true};
    const tagsPromise = Article.getTagsList();
    const locale = req.getLocale();
    res.locals.path = req.path;
    const articlesPromise = Article.find({ tags: tagQuery}).sort({updated: -1});
    const [tags, articles] = await Promise.all([tagsPromise, articlesPromise]);
    res.render('allarticles', { tags, title: 'News', tag, articles, locale, path: req.path});
};

exports.updateFiles = async (req, res) => {
    const updatedFile = req.params.updatedFile;
    // find the stallion
    const article = await Article.findOne({ _id: req.params.id });
   
    try {
        // remove file from DB
            var url = req.url;
            var pairs = url.substring(url.indexOf('+') + 1).split(',');
            req.body.url = url;
            req.body.updatedFile = pairs;
            const horse = await Article.findOneAndUpdate(
                { _id: req.params.id },
                req.body,
                { new: true, runValidators: true, context: 'query'}
            ).exec();

    }
    catch (ex) {
        res.status(500);
        return res.json({
            error: ex.message
        });
    }

    res.json({ success: true });
}

exports.removeFile = async (req, res) => {
const { id, type, type2, fileName } = req.params;
// find the stallion
const article = await Article.findOne({ _id: id });
// validate file existence
if (!article) {
    res.status(404);
    return res.json({
        error: 'article was not found.'
    });
}

// check for field and file existence in horse
if (!Array.isArray(article[type]) || article[type].length === 0 || !article[type].includes(fileName)) {
    res.status(404);
    return res.json({
        error: `File ${fileName} was not forund in the article.`
    });
}

try {
    // remove file from DB
    article[type] = article[type].filter(file => file !== fileName);
    article[type2] = article[type2].filter(updatedFile => updatedFile !== fileName);
    // wait for save
    await article.save({ validateBeforeSave: false });
    // remove the file
    fs.unlinkSync(`./public/uploads/${fileName}`);
}
catch (ex) {
    res.status(500);
    return res.json({
        error: ex.message
    });
}

res.json({ success: true });
}