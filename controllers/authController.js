const passport = require('passport');
const crypto = require('crypto');
const mongoose = require('mongoose');
const User = mongoose.model('User');
const promisify = require('es6-promisify');
const mail = require('../handlers/mail');
const mailContact = require('../handlers/mailContact')

exports.login = passport.authenticate('local', {
  failureRedirect: '/en/login',
  failureFlash: 'Failed Login!',
  successRedirect: '/nl/admin',
  successFlash: 'You are now logged in!'
});

exports.logout = (req, res) => {
  req.logout();
  req.flash('success', 'You are now logged out! 👋');
  res.redirect('/');
};

exports.isLoggedIn = (req, res, next) => {
  // first check if the user is authenticated
  if (req.isAuthenticated()) {
    next(); // carry on! They are logged in!
    return;
  }
  const locale = req.getLocale();
  req.flash('error', 'Oops you must be logged in to do that!');
  res.redirect(`/${locale}/login`);
};

exports.forgot = async (req, res) => {
  // 1. See if a user with that email exists
  const user = await User.findOne({ email: req.body.email });
  if (!user) {
    const locale = req.getLocale();
    req.flash('error', 'No account with that email exists.');
    return res.redirect(`/${locale}/login`);
  }
  // 2. Set reset tokens and expiry on their account
  user.resetPasswordToken = crypto.randomBytes(20).toString('hex');
  user.resetPasswordExpires = Date.now() + 3600000; // 1 hour from now
  await user.save();
  // 3. Send them an email with the token
  const resetURL = `http://${req.headers.host}/en/account/reset/${user.resetPasswordToken}`;
  await mail.send({
    user,
    filename: 'password-reset',
    subject: 'Password Reset',
    resetURL
  });
  req.flash('success', `You have been emailed a password reset link.`);
  // 4. redirect to login page
  const locale = req.getLocale();
  res.redirect(`/${locale}/login`);
};

exports.sendMail = async (req, res) => {
  await mailContact.send({
      to: "nathaliedemartin@hotmail.com",
      email: req.body.email,
      sender: req.body.sender,
      phone: req.body.phone,
      filename: 'contact',
      subject: `Contact Form: Balou Du Reventon`,
      message: req.body.message
    });
    req.flash('success', `Your email has been send.`);
    // 4. redirect to login page
    res.redirect('/');
};

exports.reset = async (req, res) => {
  const user = await User.findOne({
    resetPasswordToken: req.params.token,
    resetPasswordExpires: { $gt: Date.now() }
  });
  if (!user) {
    const locale = req.getLocale();
    req.flash('error', 'Password reset is invalid or has expired');
    return res.redirect(`/${locale}/login`);
  }
  // if there is a user, show the rest password form
  res.render('reset', { title: 'Reset your Password' });
};

exports.confirmedPasswords = (req, res, next) => {
  if (req.body.password === req.body['password-confirm']) {
    next(); // keepit going!
    return;
  }
  req.flash('error', 'Passwords do not match!');
  res.redirect('back');
};

exports.update = async (req, res) => {
  const user = await User.findOne({
    resetPasswordToken: req.params.token,
    resetPasswordExpires: { $gt: Date.now() }
  });

  if (!user) {
    const locale = req.getLocale();
    req.flash('error', 'Password reset is invalid or has expired');
    return res.redirect(`/${locale}/login`);
  }

  const setPassword = promisify(user.setPassword, user);
  await setPassword(req.body.password);
  user.resetPasswordToken = undefined;
  user.resetPasswordExpires = undefined;
  const updatedUser = await user.save();
  await req.login(updatedUser);
  req.flash('success', '💃 Nice! Your password has been reset! You are now logged in!');
  res.redirect('/');
};
