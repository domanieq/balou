const mongoose = require('mongoose');
const Review = mongoose.model('Review');
const Stallion = mongoose.model('Stallion');

exports.addReview = async (req, res) => {
    req.body.author = req.user._id;
    req.body.stallion = req.params.id;
    const newReview = new Review(req.body);
    await newReview.save();
    req.flash('success', 'Successfully saved your review!');
    res.redirect('back');

};