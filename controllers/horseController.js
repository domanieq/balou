const mongoose = require('mongoose');
const Horse = mongoose.model('Horse');
const Article = mongoose.model('Article');
const User = mongoose.model('User');
const Vet = mongoose.model('Vet');
const multer = require('multer');
const jimp = require('jimp');
const uuid = require('uuid');
const moment = require('moment');
const fs = require('fs');
const getYoutubeID = require('get-youtube-id');
const buildSearchQuery = require('../helpers/search-query');
const escapeStringRegexp = require('escape-string-regexp');

const multerOptions = {
    storage: multer.memoryStorage(),
    fileFilter(req, file, next) {
      const isPhoto = file.mimetype.startsWith('image/');
      if(isPhoto) {
        next(null, true);
      } else {
        next({ message: 'That filetype isn\'t allowed!' }, false);
      }
    }
  };

exports.homePage = async (req, res) => {
        const horsesPromise = Horse
            .find({"homepage":"Ja"})
            .sort({ created: 'desc'});
        const articlesPromise = Article.find().sort({updated: -1});
        const resultsPromise = Result.find().sort({updated: -1});
        const [horses, articles, results] = await Promise.all([horsesPromise, articlesPromise, resultsPromise])
        res.render('index', { title: 'Index', horses, articles, results });
    };

exports.getStabling = async (req, res) => {
        const horsesPromise = Horse
            .find()
            .sort({ created: 'desc'});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles] = await Promise.all([horsesPromise, articlesPromise])
        res.render('stabling', { title: 'Index', horses, articles });
    };

exports.getAllhorses = async (req, res) => {
        const horsesPromise = Horse
            .find()
            .sort({ created: 'desc'});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles] = await Promise.all([horsesPromise, articlesPromise])
        res.render('allhorses', { title: 'All horses', horses, articles });
    };

exports.getRearing = async (req, res) => {
        const horsesPromise = Horse
            .find()
            .sort({ created: 'desc'});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles] = await Promise.all([horsesPromise, articlesPromise])
        res.render('rearing', { horses, articles });
    };

exports.getOpfok = async (req, res) => {
        const horsesPromise = Horse
            .find()
            .sort({ created: 'desc'});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles] = await Promise.all([horsesPromise, articlesPromise])
        res.render('rearing', { horses, articles });
    };

exports.getStallions = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Dekhengst"})
            .sort({ created: 'desc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Dekhengst"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('stallions', { title: 'Stallions', horses, articles, count, pages });
    };
 
exports.getSporthorses = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Sportpaard"})
            .sort({ created: 'desc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Sportpaard"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('sportpaarden', { title: 'Sporthorses', horses, articles, count, pages });
    };

exports.getDressageSporthorses = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;
    
        const horsesPromise = Horse
            .find({ $and:[ {'category':'Sportpaard'}, {'category':'Dressuur'} ]})
            .sort({ created: 'desc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({ $and:[ {'category':'Sportpaard'}, {'category':'Dressuur'} ]});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('foals', { title: 'Sporthorses', horses, articles, count, pages });
    };

exports.getSporthorsesByLevel = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Sportpaard"})
            .sort({ level: 'desc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Sportpaard"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('foals', { title: 'Sporthorses', horses, articles, count, pages });
    };

exports.getSporthorsesByLevelZ = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Sportpaard"})
            .sort({ level: 'asc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Sportpaard"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('foals', { title: 'Sporthorses', horses, articles, count, pages });
    };

exports.getSporthorsesByName = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Sportpaard"})
            .sort({ birthname: 'asc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Sportpaard"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('foals', { title: 'Sporthorses', horses, articles, count, pages });
    };

exports.getSporthorsesByNameZ = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Sportpaard"})
            .sort({ birthname: 'desc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Sportpaard"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('foals', { title: 'Sporthorses', horses, articles, count, pages });
    };

exports.getSporthorsesByAge = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Sportpaard"})
            .sort({ dateOfBirth: 'asc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Sportpaard"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('foals', { title: 'Sporthorses', horses, articles, count, pages });
    };

exports.getSporthorsesByAgeZ = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Sportpaard"})
            .sort({ dateOfBirth: 'desc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Sportpaard"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('foals', { title: 'Sporthorses', horses, articles, count, pages });
    };
exports.getDressageHorses = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;
    
        const horsesPromise = Horse
            .find({'category':'Dressuur'})
            .sort({ created: 'asc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({'category':'Dressuur'});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('foals', { title: 'Dressage Horses', horses, articles, count, pages });
    };


exports.getFokmerries = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 121;
    const skip = (page * limit) - limit;

    const horsesPromise = Horse
        .find({"category":"Fokmerrie"})
        .sort({ dateOfBirth: 'desc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({"category":"Fokmerrie"});
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    const url = req.url;
    res.render('fokmerries', { title: 'Broodmares', horses, articles, count, pages, url });
    };

exports.getDressageBroodmares = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;
    
        const horsesPromise = Horse
            .find({ $and:[ {'category':'Fokmerrie'}, {'category':'Dressuur'} ]})
            .sort({ created: 'desc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({ $and:[ {'category':'Fokmerrie'}, {'category':'Dressuur'} ]});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('fokmerries', { title: 'Broodmares', horses, articles, count, pages });
    };

    exports.getBroodmaresByLevel = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Fokmerrie"})
            .sort({ level: 'desc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Fokmerrie"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('fokmerries', { title: 'Broodmares', horses, articles, count, pages });
    };

exports.getBroodmaresByLevelZ = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Fokmerrie"})
            .sort({ level: 'asc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Fokmerrie"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('fokmerries', { title: 'Broodmares', horses, articles, count, pages });
    };

exports.getBroodmaresByName = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Fokmerrie"})
            .sort({ birthname: 'asc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Fokmerrie"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('fokmerries', { title: 'Broodmares', horses, articles, count, pages });
    };

exports.getBroodmaresByNameZ = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Fokmerrie"})
            .sort({ birthname: 'desc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Fokmerrie"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('fokmerries', { title: 'Broodmares', horses, articles, count, pages });
    };

exports.getBroodmaresByAge = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Fokmerrie"})
            .sort({ dateOfBirth: 'asc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Fokmerrie"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('fokmerries', { title: 'Broodmares', horses, articles, count, pages });
    };

exports.getBroodmaresByAgeZ = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Fokmerrie"})
            .sort({ dateOfBirth: 'desc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Fokmerrie"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('fokmerries', { title: 'Broodmares', horses, articles, count, pages });
    };

exports.getYoungHorses = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 121;
    const skip = (page * limit) - limit;
    var dt = new Date();
    var foo = new Date();
    var bar = foo.getFullYear() -1;
    var oneYearEarlier = new Date();
    oneYearEarlier.setFullYear(bar);
    const horsesPromise = Horse
        .find({ $and: [{"dateOfBirth": { $lt: moment(dt.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie", $eq: "Jong Paard" } } ] })
        .sort({ created: 'desc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({"category":"Jong Paard"});
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('younghorses', { title: 'Young Horses', horses, articles, count, pages });
};

exports.getDressageYoungHorses = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 121;
    const skip = (page * limit) - limit;

    const horsesPromise = Horse
        .find({ $and:[ {'category':'Jong Paard'}, {'category':'Dressuur'} ]})
        .sort({ created: 'desc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({ $and:[ {'category':'Jong Paard'}, {'category':'Dressuur'} ]});
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('foals', { title: 'Young Horses', horses, articles, count, pages });
};

exports.getYoungHorsesByLevel = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Jong Paard"})
            .sort({ level: 'desc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Jong Paard"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('younghorses', { title: 'Young Horses', horses, articles, count, pages });
    };

exports.getYoungHorsesByLevelZ = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Jong Paard"})
            .sort({ level: 'asc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Jong Paard"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('younghorses', { title: 'Young Horses', horses, articles, count, pages });
    };

exports.getYoungHorsesByName = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Jong Paard"})
            .sort({ birthname: 'asc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Jong Paard"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('younghorses', { title: 'Young Horses', horses, articles, count, pages });
    };

exports.getYoungHorsesByNameZ = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Jong Paard"})
            .sort({ birthname: 'desc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Jong Paard"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('younghorses', { title: 'Young Horses', horses, articles, count, pages });
    };

exports.getYoungHorsesByAge = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Jong Paard"})
            .sort({ dateOfBirth: 'asc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Jong Paard"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('younghorses', { title: 'Young Horses', horses, articles, count, pages });
    };

exports.getYoungHorsesByAgeZ = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Jong Paard"})
            .sort({ dateOfBirth: 'desc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Jong Paard"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('younghorses', { title: 'Young Horses', horses, articles, count, pages });
    };

exports.getDressageFoals = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 50;
    const skip = (page * limit) - limit;
    var dt = new Date();
    var foo = new Date();
    var bar = foo.getFullYear() + 1;
    var oneYearLater = new Date();
    oneYearLater.setFullYear(bar);

    const horsesPromise = Horse
        .find({"category":"Dressuur", "dateOfBirth": { $gte: moment(dt.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearLater.getFullYear(), 'YYYY-MM-DD').toDate()}})
        .sort({ created: 'asc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({"category":"Dressuur","dateOfBirth": { $gte: moment(dt.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearLater.getFullYear(), 'YYYY-MM-DD').toDate()}});
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('foals', { title: 'Dressage Foals', horses, articles, count, pages });
};

exports.getDressageFoalsBySire = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Dressuurveulen"})
            .sort({ sire: 'desc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Dressuurveulen"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('foals', { title: 'Foals', horses, articles, count, pages });
    };

exports.getDressageFoalsBySireZ = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 121;
    const skip = (page * limit) - limit;

    const horsesPromise = Horse
        .find({"category":"Dressuurveulen"})
        .sort({ sire: 'asc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({"category":"Dressuurveulen"});
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('foals', { title: 'Foals', horses, articles, count, pages });
};

exports.getDressageFoalsByName = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Dressuurveulen"})
            .sort({ birthname: 'asc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Dressuurveulen"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('foals', { title: 'Foals', horses, articles, count, pages });
    };

exports.getDressageFoalsByNameZ = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 121;
    const skip = (page * limit) - limit;

    const horsesPromise = Horse
        .find({"category":"Dressuurveulen"})
        .sort({ birthname: 'desc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({"category":"Dressuurveulen"});
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('foals', { title: 'Foals', horses, articles, count, pages });
};

exports.getDressageFoalsByDam = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 121;
    const skip = (page * limit) - limit;

    const horsesPromise = Horse
        .find({"category":"Dressuurveulen"})
        .sort({ dam: 'desc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({"category":"Dressuurveulen"});
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('foals', { title: 'Foals', horses, articles, count, pages });
};

exports.getDressageFoalsByDamZ = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 121;
    const skip = (page * limit) - limit;

    const horsesPromise = Horse
        .find({"category":"Dressuurveulen"})
        .sort({ dam: 'asc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({"category":"Dressuurveulen"});
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('foals', { title: 'Foals', horses, articles, count, pages });
};

exports.getFoals = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 50;
    const skip = (page * limit) - limit;
    var dt = new Date();
    var foo = new Date();
    var bar = foo.getFullYear() + 1;
    var oneYearLater = new Date();
    oneYearLater.setFullYear(bar);

    const horsesPromise = Horse
        .find( { $and: [{"dateOfBirth": { $gte: moment(dt.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearLater.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] })
        .sort({ created: 'asc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({ $and: [{"dateOfBirth": { $gte: moment(dt.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearLater.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] });
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('foals', { title: 'Foals', horses, articles, count, pages });
};

exports.getFoalsBySire = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;
        var dt = new Date();
        var foo = new Date();
        var bar = foo.getFullYear() + 1;
        var oneYearLater = new Date();
        oneYearLater.setFullYear(bar);
    
        const horsesPromise = Horse
            .find({ $and: [{"dateOfBirth": { $gte: moment(dt.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearLater.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] })
            .sort({ sire: 'desc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Veulen"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('foals', { title: 'Foals', horses, articles, count, pages });
    };

exports.getFoalsBySireZ = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 121;
    const skip = (page * limit) - limit;
    var dt = new Date();
    var foo = new Date();
    var bar = foo.getFullYear() + 1;
    var oneYearLater = new Date();
    oneYearLater.setFullYear(bar);

    const horsesPromise = Horse
        .find({ $and: [{"dateOfBirth": { $gte: moment(dt.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearLater.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] })
        .sort({ sire: 'asc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({"category":"Veulen"});
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('foals', { title: 'Foals', horses, articles, count, pages });
};

exports.getFoalsByName = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;
        var dt = new Date();
        var foo = new Date();
        var bar = foo.getFullYear() + 1;
        var oneYearLater = new Date();
        oneYearLater.setFullYear(bar);
    
        const horsesPromise = Horse
            .find({ $and: [{"dateOfBirth": { $gte: moment(dt.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearLater.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] })
            .sort({ birthname: 'asc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Veulen"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('foals', { title: 'Foals', horses, articles, count, pages });
    };

exports.getFoalsByNameZ = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 121;
    const skip = (page * limit) - limit;
    var dt = new Date();
    var foo = new Date();
    var bar = foo.getFullYear() + 1;
    var oneYearLater = new Date();
    oneYearLater.setFullYear(bar);

    const horsesPromise = Horse
        .find({ $and: [{"dateOfBirth": { $gte: moment(dt.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearLater.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] })
        .sort({ birthname: 'desc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({"category":"Veulen"});
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('foals', { title: 'Foals', horses, articles, count, pages });
};

exports.getFoalsByDam = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 121;
    const skip = (page * limit) - limit;
    var dt = new Date();
    var foo = new Date();
    var bar = foo.getFullYear() + 1;
    var oneYearLater = new Date();
    oneYearLater.setFullYear(bar);

    const horsesPromise = Horse
        .find({ $and: [{"dateOfBirth": { $gte: moment(dt.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearLater.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] })
        .sort({ dam: 'desc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({"category":"Veulen"});
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('foals', { title: 'Foals', horses, articles, count, pages });
};

exports.getFoalsByDamZ = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 121;
    const skip = (page * limit) - limit;
    var dt = new Date();
    var foo = new Date();
    var bar = foo.getFullYear() + 1;
    var oneYearLater = new Date();
    oneYearLater.setFullYear(bar);

    const horsesPromise = Horse
        .find({ $and: [{"dateOfBirth": { $gte: moment(dt.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearLater.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] })
        .sort({ dam: 'asc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({"category":"Veulen"});
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('foals', { title: 'Foals', horses, articles, count, pages });
};

exports.getEmbryos = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;
        var foo = new Date();
        var bar = foo.getFullYear() + 1;
        var oneYearLater = new Date();
        oneYearLater.setFullYear(bar);
        const horsesPromise = Horse
            .find({ $and: [{"dateOfBirth": { $gte: moment(oneYearLater.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ]}) 
            .sort({ created: 'desc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({ $and: [{"dateOfBirth": { $gte: moment(oneYearLater.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ]});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('embryos', { title: 'Embryos', horses, articles, count, pages });
    };

exports.getEmbryosBySire = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Embryo"})
            .sort({ sire: 'desc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Embryo"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('embryos', { title: 'Embryos', horses, articles, count, pages });
    };

exports.getEmbryosBySireZ = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 121;
    const skip = (page * limit) - limit;

    const horsesPromise = Horse
        .find({"category":"Embryo"})
        .sort({ sire: 'asc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({"category":"Embryo"});
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('embryos', { title: 'Embryos', horses, articles, count, pages });
};

exports.getEmbryosByName = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Embryo"})
            .sort({ draagmoeder: 'asc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Embryo"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('embryos', { title: 'Embryos', horses, articles, count, pages });
    };

exports.getEmbryosByNameZ = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 121;
    const skip = (page * limit) - limit;

    const horsesPromise = Horse
        .find({"category":"Embryo"})
        .sort({ draagmoeder: 'desc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({"category":"Embryo"});
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('embryos', { title: 'Embryos', horses, articles, count, pages });
};

exports.getEmbryosByDam = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 121;
    const skip = (page * limit) - limit;

    const horsesPromise = Horse
        .find({"category":"Embryo"})
        .sort({ dam: 'desc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({"category":"Embryo"});
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('embryos', { title: 'Embryos', horses, articles, count, pages });
};

exports.getEmbryosByDamZ = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 121;
    const skip = (page * limit) - limit;

    const horsesPromise = Horse
        .find({"category":"Embryo"})
        .sort({ dam: 'asc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({"category":"Embryo"});
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('embryos', { title: 'Embryos', horses, articles, count, pages });
};

exports.getYearlings = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 50;
    const skip = (page * limit) - limit;
    var dt = new Date();
    var foo = new Date();
    var bar = foo.getFullYear() -1;
    var oneYearEarlier = new Date();
    oneYearEarlier.setFullYear(bar);
    //Veulens = gte 2020 en lager dan 2021
    //Jaarlingen = gte 2019 en lager dan 2020
    const horsesPromise = Horse
        .find({ $and: [{"dateOfBirth": { $gte: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(dt.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] })
        .sort({ created: 'desc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({"dateOfBirth": { $gte: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(dt.getFullYear(), 'YYYY-MM-DD').toDate()}})
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('yearlings', { title: 'Yearlings', horses, articles, count, pages });
};

exports.getYearlingsBySire = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;
        var dt = new Date();
        var foo = new Date();
        var bar = foo.getFullYear() -1;
        var oneYearEarlier = new Date();
        oneYearEarlier.setFullYear(bar);
        //Veulens = gte 2020 en lager dan 2021
        //Jaarlingen = gte 2019 en lager dan 2020
        const horsesPromise = Horse
            .find({"dateOfBirth": { $gte: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(dt.getFullYear(), 'YYYY-MM-DD').toDate()}})
            .sort({ sire: 'desc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({ $and: [{"dateOfBirth": { $gte: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(dt.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] });
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('yearlings', { title: 'Yearlings', horses, articles, count, pages });
    };

exports.getYearlingsBySireZ = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 121;
    const skip = (page * limit) - limit;
    var dt = new Date();
    var foo = new Date();
    var bar = foo.getFullYear() -1;
    var oneYearEarlier = new Date();
    oneYearEarlier.setFullYear(bar);
    //Veulens = gte 2020 en lager dan 2021
    //Jaarlingen = gte 2019 en lager dan 2020
    const horsesPromise = Horse
        .find({ $and: [{"dateOfBirth": { $gte: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(dt.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] })
        .sort({ sire: 'asc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({ $and: [{"dateOfBirth": { $gte: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(dt.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] });
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('yearlings', { title: 'Yearlings', horses, articles, count, pages });
};

exports.getYearlingsByName = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;
        var dt = new Date();
        var foo = new Date();
        var bar = foo.getFullYear() -1;
        var oneYearEarlier = new Date();
        oneYearEarlier.setFullYear(bar);
        //Veulens = gte 2020 en lager dan 2021
        //Jaarlingen = gte 2019 en lager dan 2020
        const horsesPromise = Horse
            .find({ $and: [{"dateOfBirth": { $gte: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(dt.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] })
            .sort({ birthname: 'asc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({ $and: [{"dateOfBirth": { $gte: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(dt.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] });
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('yearlings', { title: 'Yearlings', horses, articles, count, pages });
    };

exports.getYearlingsByNameZ = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 121;
    const skip = (page * limit) - limit;
    var dt = new Date();
    var foo = new Date();
    var bar = foo.getFullYear() -1;
    var oneYearEarlier = new Date();
    oneYearEarlier.setFullYear(bar);
    //Veulens = gte 2020 en lager dan 2021
    //Jaarlingen = gte 2019 en lager dan 2020
    const horsesPromise = Horse
        .find({ $and: [{"dateOfBirth": { $gte: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(dt.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] })
        .sort({ birthname: 'desc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({ $and: [{"dateOfBirth": { $gte: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(dt.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] });
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('yearlings', { title: 'Yearlings', horses, articles, count, pages });
};

exports.getYearlingsByDam = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 121;
    const skip = (page * limit) - limit;
    var dt = new Date();
    var foo = new Date();
    var bar = foo.getFullYear() -1;
    var oneYearEarlier = new Date();
    oneYearEarlier.setFullYear(bar);
    //Veulens = gte 2020 en lager dan 2021
    //Jaarlingen = gte 2019 en lager dan 2020
    const horsesPromise = Horse
        .find({ $and: [{"dateOfBirth": { $gte: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(dt.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] })
        .sort({ dam: 'desc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({ $and: [{"dateOfBirth": { $gte: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(dt.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] });
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('yearlings', { title: 'Yearlings', horses, articles, count, pages });
};

exports.getYearlingsByDamZ = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 121;
    const skip = (page * limit) - limit;
    var dt = new Date();
    var foo = new Date();
    var bar = foo.getFullYear() -1;
    var oneYearEarlier = new Date();
    oneYearEarlier.setFullYear(bar);
    //Veulens = gte 2020 en lager dan 2021
    //Jaarlingen = gte 2019 en lager dan 2020
    const horsesPromise = Horse
        .find({ $and: [{"dateOfBirth": { $gte: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(dt.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] })
        .sort({ dam: 'asc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({ $and: [{"dateOfBirth": { $gte: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(dt.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] });
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('yearlings', { title: 'Yearlings', horses, articles, count, pages });
};

exports.get2yolds = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 50;
    const skip = (page * limit) - limit;
    var dt = new Date();
    var foo = new Date();
    var bar = foo.getFullYear() -1;
    var oneYearEarlier = new Date();
    oneYearEarlier.setFullYear(bar);
    var dt2j = dt.getFullYear()-2;
    twoYearEarlier = new Date();
    twoYearEarlier.setFullYear(dt2j);
    //2j = gte 2018 en lager dan 2019
    //Jaarlingen = gte 2019 en lager dan 2020
    const horsesPromise = Horse
        .find({ $and: [{"dateOfBirth": { $gte: moment(twoYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] })
        .sort({ created: 'desc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({ $and: [{"dateOfBirth": { $gte: moment(twoYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] })
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('2-year-olds', { title: '2-year olds', horses, articles, count, pages });
};

exports.get2yoldsBySire = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;
        var dt = new Date();
        var foo = new Date();
        var bar = foo.getFullYear() -1;
        var oneYearEarlier = new Date();
        oneYearEarlier.setFullYear(bar);
        var dt2j = dt.getFullYear()-2;
        twoYearEarlier = new Date();
        twoYearEarlier.setFullYear(dt2j);
        //2j = gte 2018 en lager dan 2019
        //Jaarlingen = gte 2019 en lager dan 2020
        const horsesPromise = Horse
            .find({ $and: [{"dateOfBirth": { $gte: moment(twoYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] })
            .sort({ sire: 'desc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({ $and: [{"dateOfBirth": { $gte: moment(twoYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] });
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('2-year-olds', { title: '2-year olds', horses, articles, count, pages });
    };

exports.get2yoldsBySireZ = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 121;
    const skip = (page * limit) - limit;
    var dt = new Date();
    var foo = new Date();
    var bar = foo.getFullYear() -1;
    var oneYearEarlier = new Date();
    oneYearEarlier.setFullYear(bar);
    var dt2j = dt.getFullYear()-2;
    twoYearEarlier = new Date();
    twoYearEarlier.setFullYear(dt2j);
    //2j = gte 2018 en lager dan 2019
    //Jaarlingen = gte 2019 en lager dan 2020
    const horsesPromise = Horse
        .find({ $and: [{"dateOfBirth": { $gte: moment(twoYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] })
        .sort({ sire: 'asc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({ $and: [{"dateOfBirth": { $gte: moment(twoYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] });
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('2-year-olds', { title: '2-year olds', horses, articles, count, pages });
};

exports.get2yoldsByName = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;
        var dt = new Date();
        var foo = new Date();
        var bar = foo.getFullYear() -1;
        var oneYearEarlier = new Date();
        oneYearEarlier.setFullYear(bar);
        var dt2j = dt.getFullYear()-2;
        twoYearEarlier = new Date();
        twoYearEarlier.setFullYear(dt2j);
        //2j = gte 2018 en lager dan 2019
        //Jaarlingen = gte 2019 en lager dan 2020
        const horsesPromise = Horse
            .find({ $and: [{"dateOfBirth": { $gte: moment(twoYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] })
                .sort({ birthname: 'asc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({ $and: [{"dateOfBirth": { $gte: moment(twoYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] });
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('2-year-olds', { title: '2-year olds', horses, articles, count, pages });
    };

exports.get2yoldsByNameZ = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 121;
    const skip = (page * limit) - limit;
    var dt = new Date();
    var foo = new Date();
    var bar = foo.getFullYear() -1;
    var oneYearEarlier = new Date();
    oneYearEarlier.setFullYear(bar);
    var dt2j = dt.getFullYear()-2;
    twoYearEarlier = new Date();
    twoYearEarlier.setFullYear(dt2j);
    //2j = gte 2018 en lager dan 2019
    //Jaarlingen = gte 2019 en lager dan 2020
    const horsesPromise = Horse
        .find({ $and: [{"dateOfBirth": { $gte: moment(twoYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] })
        .sort({ birthname: 'desc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({ $and: [{"dateOfBirth": { $gte: moment(twoYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] });
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('2-year-olds', { title: '2-year olds', horses, articles, count, pages });
};

exports.get2yoldsByDam = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 121;
    const skip = (page * limit) - limit;
    var dt = new Date();
    var foo = new Date();
    var bar = foo.getFullYear() -1;
    var oneYearEarlier = new Date();
    oneYearEarlier.setFullYear(bar);
    var dt2j = dt.getFullYear()-2;
    twoYearEarlier = new Date();
    twoYearEarlier.setFullYear(dt2j);
    //2j = gte 2018 en lager dan 2019
    //Jaarlingen = gte 2019 en lager dan 2020
    const horsesPromise = Horse
        .find({ $and: [{"dateOfBirth": { $gte: moment(twoYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] })
        .sort({ dam: 'desc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({ $and: [{"dateOfBirth": { $gte: moment(twoYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] });
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('2-year-olds', { title: '2-year olds', horses, articles, count, pages });
};

exports.get2yoldsByDamZ = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 121;
    const skip = (page * limit) - limit;
    var dt = new Date();
    var foo = new Date();
    var bar = foo.getFullYear() -1;
    var oneYearEarlier = new Date();
    oneYearEarlier.setFullYear(bar);
    var dt2j = dt.getFullYear()-2;
    twoYearEarlier = new Date();
    twoYearEarlier.setFullYear(dt2j);
    //2j = gte 2018 en lager dan 2019
    //Jaarlingen = gte 2019 en lager dan 2020
    const horsesPromise = Horse
        .find({ $and: [{"dateOfBirth": { $gte: moment(twoYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] })
        .sort({ dam: 'asc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({ $and: [{"dateOfBirth": { $gte: moment(twoYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] });
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('2-year-olds', { title: '2-year olds', horses, articles, count, pages });
};

exports.getForSale = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category": "Te Koop"})
            .sort({ created: 'desc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category": "Te Koop"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('forsale', { title: 'For Sale', horses, articles, count, pages });
    };

exports.getForSaleByLevel = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category": "Te Koop"})
            .sort({ level: 'desc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category": "Te Koop"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('forsale', { title: 'For Sale', horses, articles, count, pages });
    };

exports.getForSaleByLevelZ = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category": "Te Koop"})
            .sort({ level: 'asc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category": "Te Koop"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('forsale', { title: 'For Sale', horses, articles, count, pages });
    };

exports.getForSaleByName = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category": "Te Koop"})
            .sort({ birthname: 'asc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category": "Te Koop"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('forsale', { title: 'For Sale', horses, articles, count, pages });
    };

exports.getForSaleByNameZ = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category": "Te Koop"})
            .sort({ birthname: 'desc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category": "Te Koop"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('forsale', { title: 'For Sale', horses, articles, count, pages });
    };

exports.getForSaleByAge = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category": "Te Koop"})
            .sort({ dateOfBirth: 'asc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category": "Te Koop"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('forsale', { title: 'For Sale', horses, articles, count, pages });
    };

exports.getForSaleByAgeZ = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category": "Te Koop"})
            .sort({ dateOfBirth: 'desc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category": "Te Koop"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('forsale', { title: 'For Sale', horses, articles, count, pages });
    };
    
exports.get3yolds = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 50;
    const skip = (page * limit) - limit;
    var dt = new Date();
    var foo = new Date();
    var bar = foo.getFullYear() -2;
    var oneYearEarlier = new Date();
    oneYearEarlier.setFullYear(bar);
    var dt2j = dt.getFullYear()-3;
    twoYearEarlier = new Date();
    twoYearEarlier.setFullYear(dt2j);
    //2j = gte 2018 en lager dan 2019
    //Jaarlingen = gte 2019 en lager dan 2020
    const horsesPromise = Horse
        .find({ $and: [{"dateOfBirth": { $gte: moment(twoYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] })
        .sort({ created: 'desc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({ $and: [{"dateOfBirth": { $gte: moment(twoYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] })
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('3-year-olds', { title: '3-year olds', horses, articles, count, pages });
};

exports.get3yoldsBySire = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;
        var dt = new Date();
        var foo = new Date();
        var bar = foo.getFullYear() -2;
        var oneYearEarlier = new Date();
        oneYearEarlier.setFullYear(bar);
        var dt2j = dt.getFullYear()-3;
        twoYearEarlier = new Date();
        twoYearEarlier.setFullYear(dt2j);
        //2j = gte 2018 en lager dan 2019
        //Jaarlingen = gte 2019 en lager dan 2020
        const horsesPromise = Horse
            .find({ $and: [{"dateOfBirth": { $gte: moment(twoYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] })
            .sort({ sire: 'desc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({ $and: [{"dateOfBirth": { $gte: moment(twoYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] });
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('3-year-olds', { title: '3-year olds', horses, articles, count, pages });
    };

exports.get3yoldsBySireZ = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 121;
    const skip = (page * limit) - limit;
    var dt = new Date();
    var foo = new Date();
    var bar = foo.getFullYear() -2;
    var oneYearEarlier = new Date();
    oneYearEarlier.setFullYear(bar);
    var dt2j = dt.getFullYear()-3;
    twoYearEarlier = new Date();
    twoYearEarlier.setFullYear(dt2j);
    //2j = gte 2018 en lager dan 2019
    //Jaarlingen = gte 2019 en lager dan 2020
    const horsesPromise = Horse
        .find({ $and: [{"dateOfBirth": { $gte: moment(twoYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] })
        .sort({ sire: 'asc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({ $and: [{"dateOfBirth": { $gte: moment(twoYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] });
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('3-year-olds', { title: '3-year olds', horses, articles, count, pages });
};

exports.get3yoldsByName = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;
        var dt = new Date();
        var foo = new Date();
        var bar = foo.getFullYear() -2;
        var oneYearEarlier = new Date();
        oneYearEarlier.setFullYear(bar);
        var dt2j = dt.getFullYear()-3;
        twoYearEarlier = new Date();
        twoYearEarlier.setFullYear(dt2j);
        //2j = gte 2018 en lager dan 2019
        //Jaarlingen = gte 2019 en lager dan 2020
        const horsesPromise = Horse
            .find({ $and: [{"dateOfBirth": { $gte: moment(twoYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] })
                .sort({ birthname: 'asc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({ $and: [{"dateOfBirth": { $gte: moment(twoYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] });
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('3-year-olds', { title: '3-year olds', horses, articles, count, pages });
    };

exports.get3yoldsByNameZ = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 121;
    const skip = (page * limit) - limit;
    var dt = new Date();
    var foo = new Date();
    var bar = foo.getFullYear() -2;
    var oneYearEarlier = new Date();
    oneYearEarlier.setFullYear(bar);
    var dt2j = dt.getFullYear()-3;
    twoYearEarlier = new Date();
    twoYearEarlier.setFullYear(dt2j);
    //2j = gte 2018 en lager dan 2019
    //Jaarlingen = gte 2019 en lager dan 2020
    const horsesPromise = Horse
        .find({ $and: [{"dateOfBirth": { $gte: moment(twoYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] })
        .sort({ birthname: 'desc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({ $and: [{"dateOfBirth": { $gte: moment(twoYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] });
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('3-year-olds', { title: '3-year olds', horses, articles, count, pages });
};

exports.get3yoldsByDam = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 121;
    const skip = (page * limit) - limit;
    var dt = new Date();
    var foo = new Date();
    var bar = foo.getFullYear() -2;
    var oneYearEarlier = new Date();
    oneYearEarlier.setFullYear(bar);
    var dt2j = dt.getFullYear()-3;
    twoYearEarlier = new Date();
    twoYearEarlier.setFullYear(dt2j);
    //2j = gte 2018 en lager dan 2019
    //Jaarlingen = gte 2019 en lager dan 2020
    const horsesPromise = Horse
        .find({ $and: [{"dateOfBirth": { $gte: moment(twoYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] })
        .sort({ dam: 'desc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({ $and: [{"dateOfBirth": { $gte: moment(twoYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] });
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('3-year-olds', { title: '3-year olds', horses, articles, count, pages });
};

exports.get3yoldsByDamZ = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 121;
    const skip = (page * limit) - limit;
    var dt = new Date();
    var foo = new Date();
    var bar = foo.getFullYear() -2;
    var oneYearEarlier = new Date();
    oneYearEarlier.setFullYear(bar);
    var dt2j = dt.getFullYear()-3;
    twoYearEarlier = new Date();
    twoYearEarlier.setFullYear(dt2j);
    //2j = gte 2018 en lager dan 2019
    //Jaarlingen = gte 2019 en lager dan 2020
    const horsesPromise = Horse
        .find({ $and: [{"dateOfBirth": { $gte: moment(twoYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] })
        .sort({ dam: 'asc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({ $and: [{"dateOfBirth": { $gte: moment(twoYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate(), $lt: moment(oneYearEarlier.getFullYear(), 'YYYY-MM-DD').toDate()}}, { category: { $ne: "Referentie" } } ] });
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('3-year-olds', { title: '3-year olds', horses, articles, count, pages });
};

exports.getBreedingProgram = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Future"})
            .sort({ created: 'desc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Future"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('breedingprogram', { title: 'Breeding Program', horses, articles, count, pages });
    };

exports.getReferences = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 121;
    const skip = (page * limit) - limit;

    const horsesPromise = Horse
        .find({"category":"Referentie"})
        .sort({ level: 'desc'})
        .skip(skip)
        .limit(limit);
    const countPromise = Horse.count({"category":"Referentie"});
    const articlesPromise = Article.find().sort({updated: 1});
    const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    res.render('references', { title: 'References', horses, articles, count, pages });
    };

exports.getReferencesByLevel = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Referentie"})
            .sort({ level: 'desc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Referentie"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('references', { title: 'References', horses, articles, count, pages });
    };

exports.getReferencesByLevelZ = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Referentie"})
            .sort({ level: 'asc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Referentie"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('references', { title: 'References', horses, articles, count, pages });
    };

exports.getReferencesByName = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Referentie"})
            .sort({ birthname: 'asc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Referentie"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('references', { title: 'References', horses, articles, count, pages });
    };

exports.getReferencesByNameZ = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Referentie"})
            .sort({ birthname: 'desc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Referentie"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('references', { title: 'References', horses, articles, count, pages });
    };

exports.getReferencesByAge = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Referentie"})
            .sort({ dateOfBirth: 'asc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Referentie"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('references', { title: 'References', horses, articles, count, pages });
    };

exports.getReferencesByAgeZ = async (req, res) => {
        const page = req.params.page || 1;
        const limit = 121;
        const skip = (page * limit) - limit;

        const horsesPromise = Horse
            .find({"category":"Referentie"})
            .sort({ dateOfBirth: 'desc'})
            .skip(skip)
            .limit(limit);
        const countPromise = Horse.count({"category":"Referentie"});
        const articlesPromise = Article.find().sort({updated: 1});
        const [horses, articles, count] = await Promise.all([horsesPromise, articlesPromise, countPromise]);
        const pages = Math.ceil(count / limit);
        res.render('references', { title: 'References', horses, articles, count, pages });
    };

exports.contactPage = (req, res) => {
    res.render('contact', {title: 'Contact Us'});  
 }


exports.errorPage = (req, res) => {
    res.render('404', {title: '404'});  
 }

exports.addHorse = (req,res) => {
    res.render('addHorse', {title: 'Add Horse'});
};


exports.upload = multer(multerOptions).fields([
    {
        name: 'file', maxCount: 10
    },
    {
        name: 'fileOffspring', maxCount: 10
    }
]);

exports.resize = async (req, res, next) => {
    // do not resize if nothing
    if (!req.files) {
        return next();
    }
    // regex to take file extension
    const re = /(?:\.([^.]+))?$/;
    // look by all possible file uploads
    for (let key of ['file', 'fileOffspring']) {
        const files = req.files[key];
        if (Array.isArray(files) && files.length > 0) {
            // set initial empty array
            req.body[key] = [];
            for (let file of files) {
                // take extension and build name
                const extension = re.exec(file.originalname)[1]; 
                const name = `${uuid.v4()}.${(extension || '').toLowerCase()}`;
                // resize buffer
                const resizedFile = await jimp.read(file.buffer);
                await resizedFile.resize(800, jimp.AUTO);
                await resizedFile.write(`./public/uploads/${name}`);
                // add file name to the array
                req.body[key].push(name);
            }
        }
    }

    next();
};

exports.createHorse = async (req, res, next) => {
    req.body.owner = req.user._id;

    try {
        const horse = await (new Horse(req.body)).save();
        const locale = req.getLocale();
        req.flash('success', `Successfully created ${horse.birthname}.`);
        res.redirect(`/${locale}/horse/${horse.slug}`);
    }
    catch (err) {
        const errorKeys = Object.keys(err.errors);
        errorKeys.forEach(key => req.flash('error', err.errors[key].message));
        res.render('addHorse', { title: 'Add Horse', horse: req.body });
    }
};

exports.getHorses = async (req, res) => {
    let { page = 1, limit = 12 } = req.params;
    // make it numbers
    limit = parseInt(limit, 13);
    page = parseInt(page, 13);
    // calculate skip
    const skip = (page * limit) - limit;
    // build search query
    const defaultQuery = {
        priceRange: [0, 4000],
        heightRange: [0, 200],
        levelRange: [0, 2],
        ageRange: [0, 30]
    };
    const query = buildSearchQuery(defaultQuery);
    // search
    const horsesPromise = Horse
        .find(query)
        .skip(skip)
        .limit(limit);

    const countPromise = Horse.count();
    const [horses, count] = await Promise.all([horsesPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    
    if (!horses.length && skip) {
        const locale = req.getLocale();
        req.flash('info', `I'm sorry, page ${page} does not exist. We will redirect you to page ${pages}`);
        res.redirect(`/${locale}/horses/page/${pages}`);
        return;
    }
    res.render('horses', { title: 'Horses', horses, page, pages, count });
};

const confirmOwner = (horseowner, user) => {
    if (!horseowner.owner.equals(user._id) || user.level > 10) {
        req.flash('error', `You must own a horse in order to edit him.`);
        res.redirect(`/horses`);;
    }
};

exports.editHorse = async (req,res) => {
    //const horseowner = await Horse.findOne({ _id: req.params.id });
    //confirmOwner(horseowner, req.user);
    const relatedsPromise = Horse.find().sort({ birthname: 1 });
    const horsePromise = Horse.findOne({ _id: req.params.id });
    const [horse, relateds] = await Promise.all([horsePromise, relatedsPromise])
    res.render('editHorse', { title: `Edit ${horse.birthname}`, horse, relateds})
};

exports.updateHorse = async (req, res) => {
    const { id } = req.params;

    // find the stallion
    const horse = await Horse.findOne({ _id: id });
    if (!horse) {
        return res.render('404', { title: 'Horse Not Found' });
    }

    let file = horse.file;
    let fileOffspring = horse.fileOffspring;

    // merge files
    if (Array.isArray(req.body.file) && req.body.file.length > 0) {
        file = file.concat(req.body.file);
    }
    if (Array.isArray(req.body.fileOffspring) && req.body.fileOffspring.length > 0) {
        fileOffspring = fileOffspring.concat(req.body.fileOffspring);
    }

    const convertlink= [];
    req.body.convertlink = req.body.youtube.map((url) => getYoutubeID(url));


    const updatedHorse = await Horse.findOneAndUpdate({ _id: id }, {
        ...req.body,
        file,
        fileOffspring
    }, {
        new: true,
        runValidators: true
    });
    const locale = req.getLocale();
    //const vets = Vet.findOneAndUpdate({ WHERE STALLION IS UNCHECKED OR GOT CHECKED})
    req.flash('success', `Successfully updated ${updatedHorse.birthname}.`);
    res.redirect(`/${locale}/horse/${updatedHorse.slug}`);
};

exports.getHorseBySlug = async (req,res) => {
    const horsePromise = Horse.findOne({ slug: req.params.slug }).populate('owner');
    const relatedhorse = Horse.findOne({slug: req.params.slug}, {horselist:1, _id:0});
    const [horse, relation] = await Promise.all([horsePromise, relatedhorse])
    const array = relation.horselist;
    const relateds = await Horse.find({ _id: array});
    res.locals.path = req.path;
    const locale = req.getLocale();
    res.render('horse', { horse, title: horse.birthname, horse, relateds, relation, locale, path: req.path });
};

exports.searchHorses = async (req, res) => {
    const horses = await Horse.find().or([
        {"birthname": {$regex: req.query.q, '$options' : 'i'}},
        {"sire": {$regex: req.query.q, '$options' : 'i'}},
        {"damsire": {$regex: req.query.q, '$options' : 'i'}},
        {"dam": {$regex: req.query.q, '$options' : 'i'}},
        {"description": {$regex: req.query.q, '$options' : 'i'}},
        {"descriptionEN": {$regex: req.query.q, '$options' : 'i'}}])
    res.json(horses);
};

exports.heartHorse = async(req, res)=> {
    const hearts = req.user.hearts.map(obj => obj.toString());
    const operator = hearts.includes(req.params.id) ? '$pull' : '$addToSet';
    const user = await User
        .findByIdAndUpdate(req.user._id,
            { [operator]: {hearts: req.params.id }},
            { new: true }
        );
   res.json(user);
};


exports.getTopHorses = async (req, res) => {
    const horses = await Horse.getTopHorses();
    res.json(horses);
//    res.render('review', { horses, title:' Best Horses'});
};

exports.siresAutocomplete = async (req, res) => {
    const { phrase } = req.query;
    if (phrase) {
        const q = new RegExp('^.*?' + escapeStringRegexp(phrase) + '.*$', 'i')
        const sires = await Horse.find({
            birthname: q
        }).sort({ birthname: 1 });
        res.json(sires);
    }
    else {
        res.json(sires);
    }
};

exports.damsAutocomplete = async (req, res) => {
    const { phrase } = req.query;
    if (phrase) {
        const q = new RegExp('^.*?' + escapeStringRegexp(phrase) + '.*$', 'i')
        const dams = await Horse.find({
            birthname: q
        }).sort({ birthname: 1 });
        res.json(dams);
    }
    else {
        res.json(dams);
    }
};

exports.updateFiles = async (req, res) => {
        const updatedFile = req.params.updatedFile;
        // find the stallion
        const horse = await Horse.findOne({ _id: req.params.id });
       
        try {
            // remove file from DB
                var url = req.url;
                var pairs = url.substring(url.indexOf('+') + 1).split(',');
                req.body.url = url;
                req.body.updatedFile = pairs;
                const horse = await Horse.findOneAndUpdate(
                    { _id: req.params.id },
                    req.body,
                    { new: true, runValidators: true, context: 'query'}
                ).exec();

        }
        catch (ex) {
            res.status(500);
            return res.json({
                error: ex.message
            });
        }
    
        res.json({ success: true });
    }
    
exports.removeFile = async (req, res) => {
    const { id, type, type2, fileName } = req.params;
    // find the stallion
    const horse = await Horse.findOne({ _id: id });
    // validate file existence
    if (!horse) {
        res.status(404);
        return res.json({
            error: 'Horse was not found.'
        });
    }

    // check for field and file existence in horse
    if (!Array.isArray(horse[type]) || horse[type].length === 0 || !horse[type].includes(fileName)) {
        res.status(404);
        return res.json({
            error: `File ${fileName} was not forund in the Stallion.`
        });
    }
   
    try {
        // remove file from DB
        horse[type] = horse[type].filter(file => file !== fileName);
        horse[type2] = horse[type2].filter(updatedFile => updatedFile !== fileName);
        // wait for save
        await horse.save({ validateBeforeSave: false });
        // remove the file
        fs.unlinkSync(`./public/uploads/${fileName}`);
    }
    catch (ex) {
        res.status(500);
        return res.json({
            error: ex.message
        });
    }

    res.json({ success: true });
}