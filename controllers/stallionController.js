const mongoose = require('mongoose');
const Stallion = mongoose.model('Stallion');
const Article = mongoose.model('Article');
const User = mongoose.model('User');
const Vet = mongoose.model('Vet');
const multer = require('multer');
const jimp = require('jimp');
const uuid = require('uuid');
const buildSearchQuery = require('../helpers/search-query');
const escapeStringRegexp = require('escape-string-regexp');

const multerOptions = {
    storage: multer.memoryStorage(),
    fileFilter(req, file, next) {
      const isPhoto = file.mimetype.startsWith('image/');
      if(isPhoto) {
        next(null, true);
      } else {
        next({ message: 'That filetype isn\'t allowed!' }, false);
      }
    }
  };

exports.homePage = async (req, res) => {
        const stallionsPromise = Stallion
            .find()
            .sort({ created: 'desc'});
        const articlesPromise = Article.find().sort({updated: 1});
        const [stallions, articles] = await Promise.all([stallionsPromise, articlesPromise])
        res.render('index', { title: 'Index', stallions, articles });
    };

exports.contactPage = (req, res) => {
    res.render('contact', {title: 'Contact Us'});  
 }

exports.errorPage = (req, res) => {
    res.render('404', {title: '404'});  
 }

exports.addStallion = (req,res) => {
    res.render('editStallion', {title: 'Add Stallion'});
};


exports.upload = multer(multerOptions).single("file[]");

exports.resize = async (req, res, next) => {
    if (!req.file) {
        next();
        return;
    }
    const extension = req.file.mimetype.split('/')[1];
    req.body.file = `${uuid.v4()}.${extension}`;
    const file = await jimp.read(req.file.buffer);
    await file.resize(800, jimp.AUTO);
    await file.write(`./public/uploads/${req.body.file}`);
    next();
};

exports.createStallion = async (req,res) => {
    req.body.owner = req.user._id;
    const stallion = await (new Stallion(req.body)).save();
    req.flash('success', `Successfully created ${stallion.birthname}. Would  you like to submit his breeding qualities?`);
    res.redirect(`/stallion/${stallion.slug}`);
};

exports.getStallions = async (req, res) => {
    let { page = 1, limit = 12 } = req.params;
    // make it numbers
    limit = parseInt(limit, 13);
    page = parseInt(page, 13);
    // calculate skip
    const skip = (page * limit) - limit;
    // build search query
    const defaultQuery = {
        priceRange: [0, 4000],
        heightRange: [140, 190],
        levelRange: [1, 1.6],
        ageRange: [0, 30]
    };
    const query = buildSearchQuery(defaultQuery);
    // search
    const stallionsPromise = Stallion
        .find(query)
        .skip(skip)
        .limit(limit);

    const countPromise = Stallion.count();
    const [stallions, count] = await Promise.all([stallionsPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    
    if (!stallions.length && skip) {
        req.flash('info', `I'm sorry, page ${page} does not exist. We will redirect you to page ${pages}`);
        res.redirect(`/stallions/page/${pages}`);
        return;
    }
    res.render('stallions', { title: 'Stallions', stallions, page, pages, count });
};

const confirmOwner = (stallionowner, user) => {
    if (!stallionowner.owner.equals(user._id) || user.level > 10) {
        req.flash('error', `You must own a stallion in order to edit him.`);
        res.redirect(`/stallions`);;
    }
};

exports.editStallion = async (req,res) => {
    const stallionowner = await Stallion.findOne({ _id: req.params.id });
    confirmOwner(stallionowner, req.user);
    const vetsPromise = Vet.find();
    const stallionPromise = Stallion.findOne({ _id: req.params.id });
    const [stallion, vets] = await Promise.all([stallionPromise, vetsPromise])
    res.render('editStallion', { title: `Edit ${stallion.birthname}`, stallion, vets})
};

exports.updateStallion = async (req,res) => {
    req.body.location.type = 'Point';
    const stallion = await Stallion.findOneAndUpdate({ _id: req.params.id}, req.body, {
        new: true,
        runValidators: true,
    }).exec();
    //const vets = Vet.findOneAndUpdate({ WHERE STALLION IS UNCHECKED OR GOT CHECKED})
    req.flash('success', `Successfully updated ${stallion.birthname}.`);
    res.redirect(`/stallion/${stallion.slug}`);
};

exports.getStallionBySlug = async (req,res) => {
    const averagesPromise = Stallion.getAverageStallion();
    const vetsPromise = Vet.find();
    const stallionPromise = Stallion.findOne({ slug: req.params.slug }).populate('owner reviews commentaar orders');
    const [stallion, vets, averages] = await Promise.all([stallionPromise, vetsPromise, averagesPromise]);
    res.render('stallion', { stallion, title: stallion.birthname, stallion, averages, vets });
};

exports.searchStallions = async (req, res) => {
    let { page = 1, limit = 12 } = req.query;
    // make it numbers
    limit = parseInt(limit, 13);
    page = parseInt(page, 13);
    // calculate skip
    const skip = (page * limit) - limit;
    // build search query
    const query = buildSearchQuery(req.query);
    // search
    const stallionsPromise = Stallion.find(query).skip(skip).limit(limit);
    // count
    const countPromise = Stallion.count(query);
    // execute
    const [stallions, count] = await Promise.all([stallionsPromise, countPromise]);
    const pages = Math.ceil(count / limit);
    // return
    res.json({ stallions, page, pages, count });
};

exports.heartStallion = async(req, res)=> {
    const hearts = req.user.hearts.map(obj => obj.toString());
    const operator = hearts.includes(req.params.id) ? '$pull' : '$addToSet';
    const user = await User
        .findByIdAndUpdate(req.user._id,
            { [operator]: {hearts: req.params.id }},
            { new: true }
        );
   res.json(user);
};


exports.getTopStallions = async (req, res) => {
    const stallions = await Stallion.getTopStallions();
    res.json(stallions);
//    res.render('review', { stallions, title:' Best Stallions'});
};

exports.siresAutocomplete = async (req, res) => {
    const { phrase } = req.query;
    if (phrase) {
        const q = new RegExp('^.*?' + escapeStringRegexp(phrase) + '.*$', 'i')
        const sires = await Stallion.find({
            birthname: q
        }).sort({ birthname: 1 });
        res.json(sires);
    }
    else {
        res.json(sires);
    }
};