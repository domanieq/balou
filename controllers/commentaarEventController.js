const mongoose = require('mongoose');
const CommentaarEvent = mongoose.model('CommentaarEvent');
const Event = mongoose.model('Event');

exports.addCommentEvent = async (req, res) => {
    req.body.author = req.user._id;
    req.body.event = req.params.id;
    const newCommentEvent = new CommentaarEvent(req.body);
    await newCommentEvent.save();
    req.flash('success', 'Successfully saved your Comment!');
    res.redirect('back');

};