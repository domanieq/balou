const mongoose = require('mongoose');
const Order = mongoose.model('Order');
const Stallion = mongoose.model('Stallion');
const User = mongoose.model('User');
const Mare = mongoose.model('Mare');
const multer = require('multer');
const jimp = require('jimp');
const uuid = require('uuid');
const mailOrder = require('../handlers/mailOrder');

const multerOptions = {
    storage: multer.memoryStorage(),
    fileFilter(req,file,next) {
        const isPhoto = file.mimetype.startsWith('image/');
        if(isPhoto) {
            next(null, true);
        } else {
            next({ message: 'That filetype is not allowed!'})
        }
    }
};

exports.upload = multer(multerOptions).single("file[]");

exports.resize = async (req, res, next) => {
    if (!req.file) {
        next();
        return;
    }
    const extension = req.file.mimetype.split('/')[1];
    req.body.file = `${uuid.v4()}.${extension}`;
    const file = await jimp.read(req.file.buffer);
    await file.resize(800, jimp.AUTO);
    await file.write(`./public/uploads/orders/${req.body.file}`);
    next();
};

exports.createOrder = async (req,res) => {
    if (req.user!==undefined) {
    req.body.author = req.user._id;
    req.body.ip = req.connection.remoteAddress;
    req.body.stallion = req.params.id;
    const order = await (new Order(req.body)).save();
    
    await mailOrder.send({
            email: req.body.emailowner,
            stallionemail: req.body.emailstallionowner,
            sender: req.body.nameowner,
            filename: 'orderSemen',
            subject: `Semen Order: "${req.body.stallionname}"`,
            stallionname: req.body.stallionname,
            sementype: req.body.sementype,
            marename: req.body.marename,
            UELN: req.body.UELN,
            dateOfBirthMare: req.body.dateOfBirthMare,
            studbookmare: req.body.studbookmare,
            siremare: req.body.siremare,
            damsiremare: req.body.damsiremare,
            file: req.body.file,
            insemination: req.body.insemination,
            previous: req.body.previous,
            etoption: req.body.etoption,
            breedingass: req.body.breedingass,
            membershipnr: req.body.membershipnr,
            delivery: req.body.delivery,
            deliveryDate: req.body.deliveryDate,
            shipoption: req.body.shipoption,
            locationother: req.body.locationother.address,
            locationowner: req.body.locationowner.address,
            VAT: req.body.VAT,
            phoneowner1: req.body.phoneowner1,
            phoneowner2: req.body.phoneowner2,
            namevet: req.body.namevet,
            locationvet: req.body.locationvet.address,
            phonevet1: req.body.phonevet1,
            phonevet2: req.body.phonevet2,
            emailvet: req.body.emailvet,
            comments: req.body.comments
          });
    req.flash('success', `Successfully recieved your order!`);
    res.redirect(`/order/${order._id}`);
    }
    else {
        var userid = "99999999aaaaaaa999999999"
        req.body.author = userid;
        req.body.ip = req.connection.remoteAddress;
        req.body.stallion = req.params.id;
        const order = await (new Order(req.body)).save();

        await mailOrder.send({
            email: req.body.emailowner,
            stallionemail: req.body.emailstallionowner,
            sender: req.body.nameowner,
            filename: 'orderSemen',
            subject: `Semen Order: "${req.body.stallionname}"`,
            stallionname: req.body.stallionname,
            sementype: req.body.sementype,
            marename: req.body.marename,
            UELN: req.body.UELN,
            dateOfBirthMare: req.body.dateOfBirthMare,
            studbookmare: req.body.studbookmare,
            siremare: req.body.siremare,
            damsiremare: req.body.damsiremare,
            file: req.body.file,
            insemination: req.body.insemination,
            previous: req.body.previous,
            etoption: req.body.etoption,
            breedingass: req.body.breedingass,
            membershipnr: req.body.membershipnr,
            delivery: req.body.delivery,
            deliveryDate: req.body.deliveryDate,
            shipoption: req.body.shipoption,
            locationother: req.body.locationother.address,
            locationowner: req.body.locationowner.address,
            VAT: req.body.VAT,
            phoneowner1: req.body.phoneowner1,
            phoneowner2: req.body.phoneowner2,
            namevet: req.body.namevet,
            locationvet: req.body.locationvet.address,
            phonevet1: req.body.phonevet1,
            phonevet2: req.body.phonevet2,
            emailvet: req.body.emailvet,
            comments: req.body.comments
          });
          
        req.flash('success', `Successfully recieved your order!`);
        res.redirect(`/order/${order._id}`);
    };
};


exports.orderSemen = async (req, res) => {
    if (req.user!==undefined) {
        const stallionPromise = Stallion.findOne({ _id: req.params.id }).populate('owner');
        const maresPromise = Mare.find({ owner: req.user._id});
        const [stallion, mares] = await Promise.all([stallionPromise, maresPromise]);
        res.render('orderSemen', { stallion, title: 'Order Semen', stallion, mares});
    }
    else {
        const stallion = await Stallion.findOne({ _id: req.params.id }).populate('owner');
        res.render('orderSemen', { stallion, title: 'Order Semen'});
    }
};

exports.getOrderById = async (req,res) => {
    const order = await Order.findOne({ _id: req.params.id })
    res.render('orderOverview', {order, title: 'My Order', order});
};
