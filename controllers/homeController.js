const mongoose = require('mongoose');
const Home = mongoose.model('Home');
const Horse = mongoose.model('Horse');
const Article = mongoose.model('Article');
const User = mongoose.model('User');
const multer = require('multer');
const jimp = require('jimp');
const uuid = require('uuid');
const fs = require('fs');

const multerOptions = {
    storage: multer.memoryStorage(),
    fileFilter(req, file, next) {
      const isPhoto = file.mimetype.startsWith('image/');
      if(isPhoto) {
        next(null, true);
      } else {
        next({ message: 'That filetype isn\'t allowed!' }, false);
      }
    }
  };

exports.homePage = async (req, res) => {
    const homePromise = Home.findOne({ _id: "5eebd3b6a54a4b2f5c3af39d" })
    const horsesPromise = Horse
        .find({"homepage":"Ja"})
        .sort({ created: 'desc'});
    const articlesPromise = Article.find().sort({updated: -1});
    const stallionPromise = Horse.findOne({ _id: "6064eeaafc8b7c492c16d911" });
    const [horses, articles, home, stallion] = await Promise.all([horsesPromise, articlesPromise, homePromise, stallionPromise])
    res.render('index', { title: 'Index', horses, articles, home, stallion }); 
}

exports.upload = multer(multerOptions).fields([
    {
        name: 'file', maxCount: 10
    },
    {
        name: 'extra', maxCount: 10
    }
]);

exports.resize = async (req, res, next) => {
    // do not resize if nothing
    if (!req.files) {
        return next();
    }
    // regex to take file extension
    const re = /(?:\.([^.]+))?$/;
    // look by all possible file uploads
    for (let key of ['file', 'fileOffspring']) {
        const files = req.files[key];
        if (Array.isArray(files) && files.length > 0) {
            // set initial empty array
            req.body[key] = [];
            for (let file of files) {
                // take extension and build name
                const extension = re.exec(file.originalname)[1]; 
                const name = `${uuid.v4()}.${(extension || '').toLowerCase()}`;
                // resize buffer
                const resizedFile = await jimp.read(file.buffer);
                await resizedFile.resize(800, jimp.AUTO);
                await resizedFile.write(`./public/uploads/${name}`);
                // add file name to the array
                req.body[key].push(name);
            }
        }
    }

    next();
};

exports.addHome = (req,res) => {
    res.render('addHome', {title: 'Add Home'});
};

exports.createHome = async (req,res) => {
    req.body.owner = req.user._id;
    const locale = req.getLocale();
    const home = await (new Home(req.body)).save();
    req.flash('success', `Successfully created ${home.title}. Take a look here!`);
    res.redirect(`/${locale}/home`);
};

exports.getArticles = async (req, res) => {
    const articles = await (Home
        .find()
        .sort({ updated: -1})
        .limit(2)
    );
    res.locals.path = req.path;
    res.render('articles', { title: 'News', articles,  path: req.path });
};

const confirmOwner = (home, user) => {
    if (!home.owner.equals(user._id) || user.level > 10) {
        req.flash('error', `You must be an editor in order to add news.`);
        res.redirect(`/news`);
    }
};

exports.editHome = async (req,res) => {
    const id = "5eebd3b6a54a4b2f5c3af39d";
    const home = await Home.findOne({ _id: "5eebd3b6a54a4b2f5c3af39d" });
    confirmOwner(home, req.user);
    res.render('editHome', { title: `Edit Home`, home})
};

exports.updateeeeHome = async (req,res) => {
    const home = await Home.findOneAndUpdate({ _id: req.params.id}, req.body, {
        new: true,
        runValidators: true,
    }).exec();
    const locale = req.getLocale();
    req.flash('success', `Successfully updated Home. <a href="/nl/home">View Home</a>`);
    res.redirect(`/${locale}/home`);
};

exports.updateHome = async (req, res) => {
    const { id } = req.params;

    // find the stallion
    const home = await Home.findOne({ _id: id });
    if (!home) {
        return res.render('404', { title: 'Horse Not Found' });
    }

    let file = home.file;
    let updatedFile = home.updatedFile;
    let fileOffspring = home.fileOffspring;

    // merge files
    if (Array.isArray(req.body.file) && req.body.file.length > 0) {
        file = file.concat(req.body.file);
        updatedFile = updatedFile.concat(req.body.file);

    }
    if (Array.isArray(req.body.fileOffspring) && req.body.fileOffspring.length > 0) {
        fileOffspring = fileOffspring.concat(req.body.fileOffspring);
    }

    const updatedHome = await Home.findOneAndUpdate({ _id: id }, {
        ...req.body,
        file,
        updatedFile,
        fileOffspring
    }, {
        new: true,
        runValidators: true
    });
    const locale = req.getLocale();
    //const vets = Vet.findOneAndUpdate({ WHERE STALLION IS UNCHECKED OR GOT CHECKED})
    req.flash('success', `Successfully updated.`);
    res.redirect(`/${locale}/home`);
};

exports.getArticleBySlug = async (req,res) => {
    const home = await Home.findOne({ slug: req.params.slug }).populate('owner');
    const last5articles = await (Home
        .find()
        .sort({ updated: 1})
        .limit(5)
    );
    const locale = req.getLocale();
    if (!home) return next();
    res.render('home', { home, title: home.title, last5articles, locale });
};

exports.searchArticles = async (req,res) => {
    const articles = await Home.find({
        $text: {
            $search: req.query.q
        }
    }, {
            score: { $meta: 'textScore'}
    }).sort({
        score: { $meta: 'textScore'}
    });//.limit(5)
    res.json(articles);
};

exports.searchTest = async (req,res) => {
    const articles = await Home.find({
        $text: {
            $search: req.query.q
        }
    }, {
            score: { $meta: 'textScore'}
    }).sort({
        score: { $meta: 'textScore'}
    });//.limit(5)
    res.json(articles);
};

exports.getArticlesByTag = async (req,res) => {
    const tag = req.params.tag;
    const tagQuery = tag || { $exists: true};
    const tagsPromise = Home.getTagsList();
    const locale = req.getLocale();
    res.locals.path = req.path;
    const articlesPromise = Home.find({ tags: tagQuery}).sort({updated: -1});
    const [tags, articles] = await Promise.all([tagsPromise, articlesPromise]);
    res.render('articles', { tags, title: 'News', tag, articles, locale, path: req.path});
};

exports.getArticlesByTitle = async (req,res) => {
    const tag = req.params.tag;
    const tagQuery = tag || { $exists: true};
    const tagsPromise = Home.getTagsList();
    const locale = req.getLocale();
    res.locals.path = req.path;
    const articlesPromise = Home.find({ tags: tagQuery}).sort({updated: 1});
    const [tags, articles] = await Promise.all([tagsPromise, articlesPromise]);
    res.render('allarticles', { tags, title: 'News', tag, articles, locale, path: req.path});
};
exports.updateHorse = async (req, res) => {
    const { id } = req.params;

    // find the stallion
    const horse = await Horse.findOne({ _id: id });
    if (!horse) {
        return res.render('404', { title: 'Horse Not Found' });
    }

    let file = horse.file;
    let fileOffspring = horse.fileOffspring;

    // merge files
    if (Array.isArray(req.body.file) && req.body.file.length > 0) {
        file = file.concat(req.body.file);
    }
    if (Array.isArray(req.body.fileOffspring) && req.body.fileOffspring.length > 0) {
        fileOffspring = fileOffspring.concat(req.body.fileOffspring);
    }

    const convertlink= [];
    req.body.convertlink = req.body.youtube.map((url) => getYoutubeID(url));


    const updatedHorse = await Horse.findOneAndUpdate({ _id: id }, {
        ...req.body,
        file,
        fileOffspring
    }, {
        new: true,
        runValidators: true
    });
    const locale = req.getLocale();
    //const vets = Vet.findOneAndUpdate({ WHERE STALLION IS UNCHECKED OR GOT CHECKED})
    req.flash('success', `Successfully updated ${updatedHorse.birthname}.`);
    res.redirect(`/${locale}/horse/${updatedHorse.slug}`);
};
exports.updateFiles = async (req, res) => {
    const updatedFile = req.params.updatedFile;
    // find the stallion
    const home = await Home.findOne({ _id: req.params.id });
   
    try {
        // move file from DB
            var url = req.url;
            var pairs = url.substring(url.indexOf('+') + 1).split(',');
            req.body.url = url;
            req.body.updatedFile = pairs;
            const horse = await Home.findOneAndUpdate(
                { _id: req.params.id },
                req.body,
                { new: true, runValidators: true, context: 'query'}
            ).exec();

    }
    catch (ex) {
        res.status(500);
        return res.json({
            error: ex.message
        });
    }

    res.json({ success: true });
}

exports.removeFile = async (req, res) => {
const { id, type, type2, fileName } = req.params;
// find the stallion
const home = await Home.findOne({ _id: id });
// validate file existence
if (!home) {
    res.status(404);
    return res.json({
        error: 'home was not found.'
    });
}

// check for field and file existence in horse
if (!Array.isArray(home[type]) || home[type].length === 0 || !home[type].includes(fileName)) {
    res.status(404);
    return res.json({
        error: `File ${fileName} was not found in the home.`
    });
}

try {
    // remove file from DB
    home[type] = home[type].filter(file => file !== fileName);
    home[type2] = home[type2].filter(updatedFile => updatedFile !== fileName);
    // wait for save
    await home.save({ validateBeforeSave: false });
    // remove the file
    fs.unlinkSync(`./public/uploads/${fileName}`);
}
catch (ex) {
    res.status(500);
    return res.json({
        error: ex.message
    });
}

res.json({ success: true });
}