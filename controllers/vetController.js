const mongoose = require('mongoose');
const Vet = mongoose.model('Vet');
const Stallion = mongoose.model('Stallion');
const multer = require('multer');
const jimp = require('jimp');
const uuid = require('uuid');

const multerOptions = {
    storage: multer.memoryStorage(),
    fileFilter(req,file,next) {
        const isPhoto = file.mimetype.startsWith('image/');
        if(isPhoto) {
            next(null, true);
        } else {
            next({ message: 'That filetype is not allowed!'})
        }
    }
};

exports.homePage = (req, res) => {
   res.render('index', {title: 'Index'});  
}

// exports.addVet = (req,res) => {
//     res.render('editVet', {title: 'Add Breeding Center'});
// };

exports.addVet = async (req,res) => {
    const stallions = await (Stallion.find());
    res.render('editVet', {title: 'Add Breeding Center', stallions});
};

exports.upload = multer(multerOptions).single("file[]");

exports.resize = async (req, res, next) => {
    if (!req.file) {
        next();
        return;
    }
    const extension = req.file.mimetype.split('/')[1];
    req.body.file = `${uuid.v4()}.${extension}`;
    const file = await jimp.read(req.file.buffer);
    await file.resize(800, jimp.AUTO);
    await file.write(`./public/uploads/${req.body.file}`);
    next();
};

exports.createVet = async (req,res) => {
    req.body.owner = req.user._id;
    const vet = await (new Vet(req.body)).save();
   req.flash('success', `Successfully created ${vet.name}. Take a look here!`);
   res.redirect(`/centers/${vet.slug}`);
};

exports.getVets = async (req, res) => {
    const vets = await (Vet.find());
    res.render('vets', { title: 'Breeding Centers', vets });
};

const confirmOwner = (vetowner, user) => {
    if (!vetowner.owner.equals(user._id) || user.level > 10) {
        req.flash('error', `You must own a breeding center in order to edit it.`);
        res.redirect(`/centers`);
    }
};

exports.editVet = async (req,res) => {
    const vetowner = await Vet.findOne({ _id: req.params.id });
    const vetPromise = Vet.findOne({ _id: req.params.id });
    confirmOwner(vetowner, req.user);
    const stallionsPromise = Stallion.find();
    const [vet, stallions] = await Promise.all([vetPromise, stallionsPromise]);
    res.render('editVet', { title: `Edit ${vet.name}`, vet, stallions})
};

exports.updateVet = async (req,res) => {
    req.body.location.type = 'Point';
    const vetPromise = Vet.findOneAndUpdate({ _id: req.params.id}, req.body, {
        new: true,
        runValidators: true,
    }).exec();
    const stallionsPromise = Stallion.find(); //If no stallion is checked anymore it doesn't update?
    const [vet, stallions] = await Promise.all([vetPromise, stallionsPromise]);
    req.flash('success', `Successfully updated ${vet.name}. <a href="/centers/${vet.slug}">View Breeding Center</a>`);
    res.redirect(`/centers/${vet._id}/edit`);
};

exports.getVetBySlug = async (req,res) => {
    const vetPromise = Vet.findOne({ slug: req.params.slug }).populate('owner commentaarVet');
    const stallionsPromise = Stallion.find();
    const [vet, stallions] = await Promise.all([vetPromise, stallionsPromise]); 
    if (!vet) return next();
    res.render('vet', { vet, title: vet.name, vet, stallions });
};

// exports.getVetsByService = async (req, res) => {
//     const service = req.params.service;
//     const serviceQuery = service || { $exists:true};
//     const servicesPromise = Vet.getServicesList();
//     const vetsPromise = Vet.find({ services: serviceQuery});
//     const [services, vets] = await Promise.all([servicesPromise, vetsPromise]);
//     res.render('services', { services, title: 'Breeding Centers', service, vets });
// };

exports.searchVets = async (req,res) => {
    const vets = await Vet.find({
        $text: {
            $search: req.query.q,
        
        }
    }, {
            score: { $meta: 'textScore'}
    }).sort({
        score: { $meta: 'textScore'}
    });//.limit(5)
    res.json(vets);
};

exports.getVetsByService = async (req, res) => {
    const service = req.params.service;
    const serviceQuery = service || { $exists:true};
    const servicesPromise = Vet.getServicesList();
    const vetsPromise = Vet.find({ services: serviceQuery});
    const [services, vets] = await Promise.all([servicesPromise, vetsPromise]);
    res.render('services', { services, title: 'Breeding Centers', service, vets });
};

exports.getVetsByServiceMap = async (req, res) => {
    const service = req.params.service;
    const serviceQuery = service || { $exists:true};
    const servicesPromise = Vet.getServicesList();
    const vetsPromise = Vet.find({ services: serviceQuery});
    const [services, vets] = await Promise.all([servicesPromise, vetsPromise]);
    res.render('centers', { services, title: 'Breeding Centers', service, vets });
};

exports.mapVets = async (req, res) => {
    const coordinates = [req.query.lng, req.query.lat].map(parseFloat);
    const radius = req.query.maxDistance;
    const q = {
        location: {
            $near: {
                $geometry: {
                    type:'Point',
                    coordinates
                },
                $maxDistance: radius //30km
            }
        }
     };

     const vets = await Vet.find(q).select('slug name description location file').limit(10);
     res.json(vets);
};

exports.mapPage = (req, res) => {
    res.render('centers', {title: 'Map'});
};