const mongoose = require('mongoose');
const Mare = mongoose.model('Mare');
const Order = mongoose.model('Order');
const User = mongoose.model('User');
const Vet = mongoose.model('Vet');
const multer = require('multer');
const jimp = require('jimp');
const uuid = require('uuid');

const multerOptions = {
    storage: multer.memoryStorage(),
    fileFilter(req,file,next) {
        const isPhoto = file.mimetype.startsWith('image/');
        if(isPhoto) {
            next(null, true);
        } else {
            next({ message: 'That filetype is not allowed!'})
        }
    }
};


exports.addMare = (req,res) => {
    res.render('editMare', {title: 'Add Mare'});
};

exports.upload = multer(multerOptions).single("file[]");

exports.resize = async (req, res, next) => {
    if (!req.file) {
        next();
        return;
    }
    const extension = req.file.mimetype.split('/')[1];
    req.body.file = `${uuid.v4()}.${extension}`;
    const file = await jimp.read(req.file.buffer);
    await file.resize(800, jimp.AUTO);
    await file.write(`./public/uploads/${req.body.file}`);
    next();
};

exports.createMare = async (req,res) => {
    req.body.owner = req.user._id;
    const mare = await (new Mare(req.body)).save();
    req.flash('success', `Successfully created ${mare.birthname}.`);
    res.redirect(`/mare/${mare.slug}`);
};


const confirmOwner = (mareowner, user) => {
    if (!mareowner.owner.equals(user._id) || user.level > 10) {
        req.flash('error', `You must own a mare in order to edit her.`);
        res.redirect(`/`);;
    }
};

exports.editMare = async (req,res) => {
    const mareowner = await Mare.findOne({ _id: req.params.id });
    confirmOwner(mareowner, req.user);
    const vetsPromise = Vet.find();
    const marePromise = Mare.findOne({ _id: req.params.id });
    const [mare, vets] = await Promise.all([marePromise, vetsPromise])
    res.render('editMare', { title: `Edit ${mare.birthname}`, mare, vets})
};

exports.updateMare = async (req,res) => {
    req.body.location.type = 'Point';
    const mare = await Mare.findOneAndUpdate({ _id: req.params.id}, req.body, {
        new: true,
        runValidators: true,
    }).exec();
    //const vets = Vet.findOneAndUpdate({ WHERE STALLION IS UNCHECKED OR GOT CHECKED})
    req.flash('success', `Successfully updated ${mare.birthname}. <a href="/mare/${mare.slug}">View Mare</a>`);
    res.redirect(`/mare/${mare.slug}`);
};

exports.getMareBySlug = async (req,res) => {
    const ordersPromise = Order.find({ mareid: req.params.id});
    const marePromise = Mare.findOne({ slug: req.params.slug }).populate('orders');
    const [mare, orders] = await Promise.all([marePromise, ordersPromise]);
    res.render('mare', { mare, title: mare.birthname, mare, orders });
};
