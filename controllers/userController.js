const mongoose = require('mongoose');
const User = mongoose.model('User');
const Stallion = mongoose.model('Stallion');
const Order = mongoose.model('Order');
const Mare = mongoose.model('Mare');
const promisify = require('es6-promisify');
const multer = require('multer');
const jimp = require('jimp');
const uuid = require('uuid');

const multerOptions = {
    storage: multer.memoryStorage(),
    fileFilter(req,file,next) {
        const isPhoto = file.mimetype.startsWith('image/');
        if(isPhoto) {
            next(null, true);
        } else {
            next({ message: 'That filetype is not allowed!'})
        }
    }
};


exports.loginForm = (req,res) => {
    res.render('login', {title: 'Login'});
};

exports.registerForm = (req,res) => {
    res.render('register', {title: 'Register'});
};

exports.getAdmin = async (req, res) => {
    const usersPromise = User
        .find()
        .sort({ created: 'desc'});
    const [users] = await Promise.all([usersPromise])
    res.render('admin', { title: 'Index', users });
};

exports.validateRegister = (req, res, next) => {
    req.sanitizeBody('name');
    req.checkBody('name', 'You must supply a name!').notEmpty();
    req.checkBody('email', 'That email is not valid!').isEmail();
    req.sanitizeBody('email').normalizeEmail({
        remove_dots: false,
        remove_extension: false,
        gmail_remove_subaddress: false
    });
    req.checkBody('password', 'Password cannot be blank!').notEmpty();
    req.checkBody('password-confirm', 'Confirmed password cannot be blank!').notEmpty();
    req.checkBody('password-confirm', 'Oops they do not match').equals(req.body.password);

    const errors = req.validationErrors();
    if (errors) {
        req.flash('error', errors.map(err => err.msg));
        res.render('register', {title: 'Register', body: req.body, flashes: req.flash() });
        return;
}
next();
};

exports.register = async (req, res, next) => {
    const user = new User(req.body);
    const register = promisify(User.register, User);
    await register(user, req.body.password);
    next();
};

exports.account = (req,res) => {
    const hearts = req.user.hearts.map(obj => obj.toString());
    console.log(hearts);
    res.render('account', { title: 'Edit your account'});
    next();
};

exports.upload = multer(multerOptions).single("file[]");

exports.resize = async (req, res, next) => {
    if (!req.file) {
        next();
        return;
    }
    const extension = req.file.mimetype.split('/')[1];
    req.body.file = `${uuid.v4()}.${extension}`;
    const file = await jimp.read(req.file.buffer);
    await file.resize(800, jimp.AUTO);
    await file.write(`./public/uploads/${req.body.file}`);
    next();
};

exports.updateAccount = async (req, res) =>{
    req.body.location.type = 'Point';
    const updates = {
        location: req.body.location,
        coordinates: [req.body.location[0], req.body.location[1]],
        marketing: req.body.marketing,
        file: req.body.file
    }
    -console.log(req.body.file)
    -console.log(req.body.marketing)
    const user = await User.findOneAndUpdate(
        { _id: req.user._id},
        //{ $set: updates},
        req.body,
        { new: true, runValidators: true, context: 'query'}
    ).exec();
    req.flash('success', 'Successfully updated your profile!');
    res.redirect('back');
};

exports.getHearts = async (req, res) => {
    const stallionsPromise = Stallion.find({$or: 
        [
        {_id: req.user.hearts},
        {owner: req.user._id }
        ]
    });
    const ordersPromise = Order.find({author: req.user._id});
    const maresPromise = Mare.find({owner: req.user._id});
    const [stallions, orders, mares] = await Promise.all([stallionsPromise, ordersPromise, maresPromise]);
    res.render('account', {title: 'Hearted Stallions', stallions, orders, mares});
};
