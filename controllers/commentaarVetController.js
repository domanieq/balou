const mongoose = require('mongoose');
const CommentaarVet = mongoose.model('CommentaarVet');
const Vet = mongoose.model('Vet');

exports.addCommentVet = async (req, res) => {
    req.body.author = req.user._id;
    req.body.vet = req.params.id;
    const newCommentVet = new CommentaarVet(req.body);
    await newCommentVet.save();
    req.flash('success', 'Successfully saved your Comment!');
    res.redirect('back');

};