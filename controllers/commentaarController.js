const mongoose = require('mongoose');
const Commentaar = mongoose.model('Commentaar');
const Stallion = mongoose.model('Stallion');

exports.addComment = async (req, res) => {
    req.body.author = req.user._id;
    req.body.stallion = req.params.id;
    const newComment = new Commentaar(req.body);
    await newComment.save();
    req.flash('success', 'Successfully saved your Comment!');
    res.redirect('back');

};
