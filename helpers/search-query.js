const escapeStringRegexp = require('escape-string-regexp');
const moment = require('moment');

const buildSearchQuery = (filters) => {

    const { horsename, sire, siredam, damline, description, descriptionEN, semen, priceRange, ageRange } = filters;
    const query = {};

    // add text filters
    if (horsename) {
        // Stallions.birthname OR Stallions.sportname
        query.$or = [
            {
                birthname: new RegExp('^.*?' + escapeStringRegexp(horsename) + '.*$', 'i')
            },
            {
                sportname: new RegExp('^.*?' + escapeStringRegexp(horsename) + '.*$', 'i')
            }
        ];
    }

    if (sire) {
        query.sire = new RegExp('^.*?' + escapeStringRegexp(sire) + '.*$', 'i');
    }

    if (siredam) {
        query.siredam = new RegExp('^.*?' + escapeStringRegexp(siredam) + '.*$', 'i');
    }

    if (damline) {
        query.damline = new RegExp('^.*?' + escapeStringRegexp(damline) + '.*$', 'i');
    }

    if (description) {
        query.description = new RegExp('^.*?' + escapeStringRegexp(description) + '.*$', 'i');
    }

    if (descriptionEN) {
        query.descriptionEN = new RegExp('^.*?' + escapeStringRegexp(descriptionEN) + '.*$', 'i');
    }

    // filter by price => Stallions.studfee AND Stallions.studfeestraw 
    if (Array.isArray(priceRange) && priceRange.length === 2) {
        let [from, to] = priceRange.map(parseFloat);

        // set minimum price not equal to zero
        if (!from) {
            from = 1;
        }

        // if we have semen selection
        if (Array.isArray(semen) && semen.length > 0) {
            const hasFresh = semen.includes('Fresh');
            const hasFrozen = semen.includes('Frozen') || semen.includes('ICSI');

            // if one of types selected
            if (!hasFresh || !hasFrozen) {
                if (hasFresh) {
                    // if Stallions.semen = Fresh than it should filter on the values in stallions.studfee, 
                    query.$and = [
                        {
                            studfee: {
                                $gte: from
                            }
                        },
                        {
                            studfee: {
                                $lte: to
                            }
                        }
                    ];
                }
                else if (hasFrozen) {
                    // if Stallions.semen = Frozen or ICSI than it should filter on the values in stallions.studfeestraw 
                    query.$and = [
                        {
                            studfeestraw: {
                                $gte: from
                            }
                        },
                        {
                            studfeestraw: {
                                $lte: to
                            }
                        }
                    ];
                }
            }
        }

        // no selection -> use default
        if (!query.$and) {
            query.$and = [
                {
                    $or: [
                        {
                            $and: [
                                {
                                    studfee: {
                                        $gte: from
                                    }
                                },
                                {
                                    studfee: {
                                        $lte: to
                                    }
                                }
                            ]
                        },
                        {
                            $and: [
                                {
                                    studfeestraw: {
                                        $gte: from
                                    }
                                },
                                {
                                    studfeestraw: {
                                        $lte: to
                                    }
                                }
                            ]
                        }
                    ]
                }
            ];
        }
    }

    // filter by Height => stallions.height
    const ranges = ['heightRange', 'levelRange'];
    ranges.forEach((rangeName) => {
        const range = filters[rangeName];

        if (Array.isArray(range) && range.length === 2) {
            const [from, to] = range.map(parseFloat);
            // remove word "Range" to map to the DB field
            const dbFieldName = rangeName.replace('Range', '')
            const filters = [
                {
                    [dbFieldName]: {
                        $gte: from
                    }
                },
                {
                    [dbFieldName]: {
                        $lte: to
                    }
                }
            ];

            // create new $and array or push to existing
            query.$and = query.$and ? query.$and.concat(filters) : filters;
        }
    });

    // filter by age => stallions.dateOfBirth
    if (Array.isArray(ageRange) && ageRange.length === 2) {
        const [from, to] = ageRange.map(parseFloat);
        const fromDate = moment().subtract(to, 'year').startOf('year').toDate(); // older
        const toDate = moment().subtract(from, 'year').endOf('year').toDate(); // younger

        const filters = [
            {
                dateOfBirth: {
                    $gte: fromDate
                }
            },
            {
                dateOfBirth: {
                    $lte: toDate
                }
            },
        ];

        // create new $and array or push to existing
        query.$and = query.$and ? query.$and.concat(filters) : filters;
    }

    return query;
}

module.exports = buildSearchQuery;