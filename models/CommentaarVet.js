const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const commentaarVetSchema = new mongoose.Schema({
    created: {
        type: Date,
        default: Date.now
    },
    author: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: 'You must supply an author!'
    },
    vet: {
        type: mongoose.Schema.ObjectId,
        ref: 'Vet',
        required: 'You must supply a vet'
    },
    text: {
        type: String,
        required: 'Your comment must have text!'
    },
    rating: {
        type: Number,
        min: 1,
        max: 5
    }
});

function autopopulate(next) {
    this.populate('author');
    next();
}

commentaarVetSchema.pre('find', autopopulate);
commentaarVetSchema.pre('findOne', autopopulate);

module.exports = mongoose.model('CommentaarVet', commentaarVetSchema);