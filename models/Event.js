const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const slug = require('slugs');

const eventSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        required: 'Please enter a name',
    },
    slug: String,
    website: {
        type: String,
        trim: true
    },
    livestream: {
        type: String,
        trim: true
    },
    date: {
        type: Date,
        required: 'Please enter a Start Date'
    },
    enddate: {
        type: Date,
        required: 'Please enter an End Date'
    },
    updated: { 
        type: Date,
        default: Date.now 
    },
    hour: {
        type: String,
        trim: true,
        required: 'Please enter a Start time',
    },
    endhour: {
        type: String,
        trim: true,
        required: 'Please enter an End time',
    },
    facebook: {
        type: String,
        trim: true
    },
    twitter: {
        type: String,
        trim: true
    },
    location: {
        type: {
            type: String,
            default: 'Point'
        },
        coordinates: [{
            type: Number,
            required: 'Please enter the location of the event'
         }],
        address: {
            type: String,
            required: 'You must supply an address!'
        }
    },
    phone: {
        type: String,
        trim: true,
        required: 'Please enter a Phonenumber',
    },
    email: {
        type: String,
        trim: true,
        required: 'Please enter an email adress',
    },
    category: {
        type: [String],
        required: 'Please enter a category',
    },
    description_nl: {
        type: String,
        trim: true,
        required: 'Please enter a Description',
    },
    description_en: {
        type: String,
        trim: true,
        required: 'Please enter a Description',
    },
    file: {
        type: [String],
        trim: true,
        required: 'Please enter a picture',
    },
    terms: {
        type: String,
        trim: true,
        required: 'Please accept the terms & conditions',
    },
    online: Boolean,
    owner: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: 'Every event needs an owner'
    }
}, {
        toJSON: { virtuals: true},
        toObject: { virtuals: true},
    });

// define our indexes
eventSchema.index({
    name: 'text',
    description_en: 'text',
    description_nl: 'text',
    category: 'text'
});

eventSchema.pre('save', async function(next) {
    if (!this.isModified('name')) {
        next();
        return;
    }
    this.slug = slug(this.name);
    const slugRegEx = new RegExp(`^(${this.slug})((-[0-9]*$)?)$`, 'i');
    const eventsWithSlug = await this.constructor.find({ slug: slugRegEx });
    if (eventsWithSlug.length) {
        this.slug = `${this.slug}-${eventsWithSlug.length + 1}`;
    }
    next();
});

eventSchema.statics.getCategorysList = function() {
    return this.aggregate([
        { $unwind: '$category' },
        { $group: { _id: '$category', count: { $sum:1}}},
        { $sort: { count: -1}}
    ]);
};

eventSchema.virtual('commentaarEvent', {
    ref: 'CommentaarEvent',
    localField: '_id',
    foreignField: 'event'
});

function autopopulate(next) {
    this.populate('commentaarEvent');
    next();
}

eventSchema.pre('find', autopopulate);
eventSchema.pre('findOne', autopopulate);

module.exports = mongoose.model('Event', eventSchema);