const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const slug = require('slugs');

const articleSchema = new mongoose.Schema({
    title: {
        type: String,
        trim: true,
        required: 'Please enter a Tile',
    },
    title_EN: {
        type: String,
        trim: true,
        required: 'Please enter a Tile',
    },
    subtitle: {
        type: String,
        trim: true
    },
    subtitle_EN: {
        type: String,
        trim: true
    },
    slug: String,
    text_en: {
        type: String,
        trim: true,
        required: 'Please enter a Text in English',
    },
    text_nl: {
        type: String,
        trim: true,
        required: 'Please enter a Text in Dutch',
    },
    updated: { 
        type: Date,
        default: Date.now 
    },
    tags: [String],
    home_nl: {
        type: String
    },
    home_en: {
        type: String
    },
    file: {
        type: [String],
        trim: true,
        required: 'Please enter a picture',
    },
    updatedFile: {
        type: [String],
        trim: true
    },
    comments: {
        type: [String],
        trim: true,
    },
    online: Boolean,
    owner: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: 'Every article needs an owner'
    }
});

// define our indexes
articleSchema.index({
    title: 'text',
    subtitle: 'text',
    text_en: 'text',
    text_nl: 'text',
    tags: 'text'
});

articleSchema.pre('save', async function(next) {
    if (!this.isModified('title')) {
        next();
        return;
    }
    this.slug = slug(this.title);
    const slugRegEx = new RegExp(`^(${this.slug})((-[0-9]*$)?)$`, 'i');
    const articlesWithSlug = await this.constructor.find({ slug: slugRegEx });
    if (articlesWithSlug.length) {
        this.slug = `${this.slug}-${articlesWithSlug.length + 1}`;
    }
    this.home_en = this.get('text_en').split(' ').slice(0, 25).join(' ') + "..."; // considering _id is input by client
    this.home_nl = this.get('text_nl').split(' ').slice(0, 25).join(' ') + "...";
    next();
});

articleSchema.statics.getTagsList = function() {
    return this.aggregate([
        { $unwind: '$tags'},
        { $group: { _id: '$tags', count: { $sum: 1} }},
        { $sort: { count: -1 }}
    ]);
};

module.exports = mongoose.model('Article', articleSchema);