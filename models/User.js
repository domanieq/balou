const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;
const md5 = require('md5');
const validator = require('validator');
const mongdbErrorHandler = require('mongoose-mongodb-errors');
const passportLocalMongoose = require('passport-local-mongoose');

const userSchema = new Schema({
    email:{
        type: String,
        unique: true,
        lowercase: true,
        trim: true,
        validate: [validator.isEmail, 'Please enter a valid Email Address'],
        required: 'Please supply an email address'
    },
    name:{
        type: String,
        required: 'Please supply a name',
        trim: true
    },
    dateOfBirth: {
        type: Date
    },
    file: {
        type: String,
        trim: true,
    },
    marketing: String,
    userid: String,
    updated_at: { type: Date, default: Date.now },
    phone: String,
    category: String,
    terms: String,
    resetPasswordToken: String,
    resetPasswordExpires: Date,
    hearts: [
        { type: mongoose.Schema.ObjectId, ref: 'Stallion' }
    ],
    owned: [
        { type: mongoose.Schema.ObjectId, ref: 'Stallion' }
    ]
});

userSchema.virtual('gravatar').get(function() {
    const hash = md5(this.email);
    return `https://gravatar.com/avatar/${hash}?s=200`;
})

userSchema.plugin(passportLocalMongoose, {usernameField: 'email'});
userSchema.plugin(mongdbErrorHandler);

userSchema.statics.findOrCreate = require("find-or-create");

module.exports = mongoose.model('User', userSchema)