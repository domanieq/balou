const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const orderSchema = new mongoose.Schema({
    created: {
        type: Date,
        default: Date.now
    },
    author: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        //required: 'You must supply an author!'
    },
    stallion: {
        type: mongoose.Schema.ObjectId,
        ref: 'Stallion',
        required: 'You must supply a stallion'
    },
    // mareid: {
    //     type: mongoose.Schema.ObjectId,
    //     ref: 'Mare',
    //     //required: 'You must supply a stallion'
    // },
    ip: String,
    UELN: {
        type: String,
        //required: 'Your comment must have text!'
    },
    marename: {
        type: String,
        //required: 'Your mare must have a name!'
    },
    deliveryDate: {
        type: Date
    },
    delivery: {
        type: String,
        required: 'You must choose a delivery option!'
    },
    comments: String,
    shipoption: {
        type: String,
        required: 'You must choose a shipping option!'
    },
    studbookmare: {
        type: String,
        //required: 'Your mare must have a studbook!'
    },
    insemination: {
        type: String,
    },
    previous: {
        type: String,
    },
    etoption: {
        type: String,
        required: 'You must choose a value for Embryotransplantation!'
    },
    breedingass: {
        type: String,
    },
    membershipnr: {
        type: String,
    },
    siremare: {
        type: String,
       // required: 'Your mare must have a sire!'
    },
    damsiremare: {
        type: String,
       // required: 'Your mare must have a sire!'
    },
    dateOfBirthMare: {
        type: Date,
        default: 0
    },
    file: {
        type: [String],
        trim: true
    },
    nameowner: {
        type: String,
        required: 'Your mare must have an owner!'
    },
    locationowner: {
        type: {
            type: String,
            default: 'Point'
        },
        coordinates: [{
            type: Number,
            required: 'Please enter the owner\'s address'
         }],
        address: {
            type: String,
            required: 'Please enter the owner\'s address'
        }
    },
    stallionname: String,
    emailowner: {
        type: String,
        required: 'The owner must have a emailaddress!'
    },
    phoneowner1: {
        type: String,
        required: 'The owner must have a phonenumber!'
    },
    phoneowner2: {
        type: String
    },
    VAT: {
        type: String,
    },
    namevet: {
        type: String,
        required: 'The vet must have a first and last name!'
    },
    sementype: String,
    locationvet: {
        type: {
            type: String,
            default: 'Point'
        },
        coordinates: [{
            type: Number,
            required: 'Please enter the location of the vet'
         }],
        address: {
            type: String,
            required: 'Please enter the location of the vet!'
        }
    },
    deliveryLocation: {
        type: {
            type: String,
            default: 'Point'
        },
        coordinates: [{
            type: Number
         }],
        address: {
            type: String
        }
    },
    phonevet1: {
        type: String,
        required: 'The vet must have a phonenumber!'
    },
    phonevet2: {
        type: String
    },
    emailvet: {
        type: String
    },
    terms: {
        type: String,
        trim: true,
        required: 'Please accept the terms & conditions',
    }
},{
    toJSON: { virtuals: true},
    toObject: { virtuals: true},
});

// orderSchema.virtual('mare', {
//     ref: 'Mare', // what model to link?
//     localField: '_id', //which field on the stallion?
//     foreignField: 'mareid' //which field on the review?
// });

// function autopopulate(next) {
//     this.populate('author stallion');
//     next();
// }

//orderSchema.pre('find', autopopulate);
//orderSchema.pre('findOne', autopopulate);

module.exports = mongoose.model('Order', orderSchema);