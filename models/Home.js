const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const homeSchema = new mongoose.Schema({
    title: {
        type: String,
        trim: true,
        required: 'Please enter a Text',
    },
    title_EN: {
        type: String,
        trim: true,
        required: 'Please enter a Text nl',
    },
    updated: { 
        type: Date,
        default: Date.now 
    },
    file: {
        type: [String],
        trim: true,
        required: 'Please enter a picture',
    },
    updatedFile: {
        type: [String],
        trim: true
    },
    owner: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: 'Every Homepage needs an owner'
    }
});

// define our indexes
homeSchema.index({
    title: 'text'
});

homeSchema.pre('save', async function(next) {
    if (!this.isModified('title')) {
        next();
        return;
    }
    next();
});


module.exports = mongoose.model('Home', homeSchema);