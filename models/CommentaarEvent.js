const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const commentaarEventSchema = new mongoose.Schema({
    created: {
        type: Date,
        default: Date.now
    },
    author: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: 'You must supply an author!'
    },
    event: {
        type: mongoose.Schema.ObjectId,
        ref: 'Event',
        required: 'You must supply an event'
    },
    text: {
        type: String,
        required: 'Your comment must have text!'
    },
    rating: {
        type: Number,
        min: 1,
        max: 5
    }
});

function autopopulate(next) {
    this.populate('author');
    next();
}

commentaarEventSchema.pre('find', autopopulate);
commentaarEventSchema.pre('findOne', autopopulate);

module.exports = mongoose.model('CommentaarEvent', commentaarEventSchema);