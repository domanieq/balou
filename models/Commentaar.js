const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const commentaarSchema = new mongoose.Schema({
    created: {
        type: Date,
        default: Date.now
    },
    author: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: 'You must supply an author!'
    },
    stallion: {
        type: mongoose.Schema.ObjectId,
        ref: 'Stallion',
        required: 'You must supply a stallion'
    },
    text: {
        type: String,
        required: 'Your comment must have text!'
    },
    upvote: {
        type: Number,
        default: 0
    },
    downvote: {
        type: Number,
        default: 0
    }
});

function autopopulate(next) {
    this.populate('author');
    next();
}

commentaarSchema.pre('find', autopopulate);
commentaarSchema.pre('findOne', autopopulate);

module.exports = mongoose.model('Commentaar', commentaarSchema);