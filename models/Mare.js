const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const slug = require('slugs');

const mareSchema = new mongoose.Schema({
    birthname: {
        type: String,
        trim: true,
        index: true,
        required: 'Please enter a Birthname',
    },
    UELN: {
        type: String,
        required: 'Every mare needs a UELN number'
    },
    slug: String,
    sportname: {
        type: String,
        trim: true,
        index: true
    },
    sexe: {
        type: String,
        trim: true,
        index: true
    },
    height: {
        type: Number,
        trim: true,
        index: true,
        min: 1,
        max: 200
    },
    breeder: {
        type: String,
        trim: true,
        index: true,
    },
    dateOfBirth: {
        type: Date,
        index: true,
        required: 'Please enter a Date of Birth'
    },
    updated: { 
        type: Date,
        default: Date.now 
    },
    color: {
        type: String,
        trim: true,
        index: true
    },
    level: {
        type: Number,
        trim: true,
        index: true
    },
    studbook: {
        type: String,
        trim: true,
        required: 'Please enter a Studbook',
    },
    location: {
        type: {
            type: String,
            default: 'Point'
        },
        coordinates: [{
            type: Number
         }],
        address: {
            type: String
        }
    },
    wffs: {
        type: String,
        trim: true,
        index: true
    },
    redfactor: {
        type: String,
        trim: true,
        index: true
    },
    category: {
        type: [String],
        index: true
    },
    xxox: {
        type: Number,
        trim: true
    },
    approved: {
        type: [String],
        trim: true,
        index: true
    },
    description: {
        type: String,
        trim: true
    },
    result: {
        type: [String],
        trim: true,
    },
    youtube: [String],
    offspring: {
        type: [String],
        trim: true,
    },
    damline: {
        type: String,
        trim: true,
    },
    position: Number,
    feilink: {
        type: String,
        trim: true,
    },
    forsale: {
        type: Boolean,
        trim: true,
        default: false,
    },
    wffs: {
        type: String,
        trim: true,
        index: true
    },
    redfactor: {
        type: String,
        trim: true,
        index: true
    },
    sire: {
        type: String,
        trim: true,
        index: true,
        required: 'Please enter a Sire',
    },
    dam: {
        type: String,
        trim: true,
        required: 'Please enter a Dam',
    },
    siresire: {
        type: String,
        trim: true
    },
    damsire: {
        type: String,
        trim: true,
        required: 'Please enter the Sire of the Dam',
    },
    siresiresire: {
        type: String,
        trim: true
    },
    damsiresire: {
        type: String,
        trim: true
    },
    siredamsire: {
        type: String,
        trim: true
    },
    damdamsire: {
        type: String,
        trim: true
    },
    siredam: {
        type: String,
        trim: true
    },
    damdam: {
        type: String,
        trim: true
    },
    siresiredam: {
        type: String,
        trim: true,
        index: true
    },
    damsiredam: {
        type: String,
        trim: true
    },
    siredamdam: {
        type: String,
        trim: true
    },
    damdamdam: {
        type: String,
        trim: true
    },
    file: {
        type: [String],
        trim: true
    },
    fileOffspring: {
        type: [String],
        trim: true
    },
    terms: {
        type: String,
        trim: true,
        required: 'Please accept the terms & conditions',
    },
    labels: {
        type: String,
        trim: true,
    },
    owner: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: 'Every mare needs an owner'
    } 
}, {
        toJSON: { virtuals: true},
        toObject: { virtuals: true},
});

// define our indexes
mareSchema.index({
    birthname: 'text',
    sire: 'text',
    damsire: 'text'
});

mareSchema.pre('save', async function(next) {
    if (!this.isModified('birthname')) {
        next();
        return;
    }
    this.slug = slug(this.birthname);
    const slugRegEx = new RegExp(`^(${this.slug})((-[0-9]*$)?)$`, 'i');
    const maresWithSlug = await this.constructor.find({ slug: slugRegEx });
    if (maresWithSlug.length) {
        this.slug = `${this.slug}-${maresWithSlug.length + 1}`;
    }
    next();
});


module.exports = mongoose.model('Mare', mareSchema);