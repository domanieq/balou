const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const slug = require('slugs');

const stallionSchema = new mongoose.Schema({
    birthname: {
        type: String,
        trim: true,
        index: true,
        required: 'Please enter a Birthname',
    },
    slug: String,
    sportname: {
        type: String,
        trim: true,
        index: true,
        required: 'Please enter a Sportname',
    },
    height: {
        type: Number,
        trim: true,
        index: true,
        min: 1,
        max: 200,
        required: 'Please enter a Height in centimeters',
    },
    breeder: {
        type: String,
        trim: true,
        index: true,
        required: 'Please enter a Breeder',
    },
    dateOfBirth: {
        type: Date,
        index: true,
        required: 'Please enter a Date of Birth'
    },
    updated: { 
        type: Date,
        default: Date.now 
    },
    color: {
        type: String,
        trim: true,
        index: true,
        required: 'Please enter a Color',
    },
    level: {
        type: Number,
        trim: true,
        index: true,
        required: 'Please enter a Level',
    },
    studbook: {
        type: String,
        trim: true,
        required: 'Please enter a Studbook',
    },
    location: {
        type: {
            type: String,
            default: 'Point'
        },
        coordinates: [{
            type: Number,
         }],
        address: {
            type: String,
        }
    },
    lfg: {
        type: String,
        trim: true,
        index: true,
        required: 'Please enter a Life Foal Guarantuee',
    },
    wffs: {
        type: String,
        trim: true,
        index: true,
        required: 'Please enter a if the horse is WFFS carrier or not',
    },
    redfactor: {
        type: String,
        trim: true,
        index: true,
        required: 'Please enter a if the horse has Red factor or not',
    },
    category: {
        type: [String],
        index: true,
        required: 'Please choose a category for the horse'
    },
    xxox: {
        type: Number,
        trim: true
    },
    approved: {
        type: [String],
        trim: true,
        index: true,
        required: 'Please enter for which studbook the stallion is approved',
    },
    available: {
        type: [String],
        trim: true,
        index: true,
        required: 'Please enter where the stallion is available for breeding',
    },
    studfee: {
        type: Number,
        index: true,
        required: 'Please enter a Studfee',
    },
    studfeestraw: {
        type: Number,
        index: true,
        required: 'Please enter a Studfee/straw',
    },
    semen: {
        type: [String],
        trim: true,
        index: true,
        required: 'Please enter the Semen type',
    },
    semendescription: {
        type: String,
        trim: true
    },
    freshfrom: { 
        type: Date
    },
    freshuntil: { 
        type: Date
    },
    description: {
        type: String,
        trim: true,
        required: 'Please enter a Description',
    },
    result: {
        type: [String],
        trim: true,
    },
    youtube: [String],
    offspring: {
        type: [String],
        trim: true,
    },
    damline: {
        type: String,
        trim: true,
    },
    position: Number,
    feilink: {
        type: String,
        trim: true,
    },
    forsale: {
        type: String,
        trim: true,
        default: '0',
    },
    sire: {
        type: String,
        trim: true,
        index: true,
        required: 'Please enter a Sire',
    },
    dam: {
        type: String,
        trim: true,
        required: 'Please enter a Dam',
    },
    siresire: {
        type: String,
        trim: true,
        required: 'Please enter the Sire of the Sire',
    },
    damsire: {
        type: String,
        trim: true,
        required: 'Please enter the Dam of the Sire',
    },
    siresiresire: {
        type: String,
        trim: true,
        required: 'Please enter the Sire of the Sire\'s Sire',
    },
    damsiresire: {
        type: String,
        trim: true,
        required: 'Please enter the Dam of the Sire\'s Sire',
    },
    siredamsire: {
        type: String,
        trim: true,
        required: 'Please enter the Sire of the Dam\'s Sire',
    },
    damdamsire: {
        type: String,
        trim: true,
        required: 'Please enter the Dam of the Dam\'s Sire',
    },
    siredam: {
        type: String,
        trim: true,
        required: 'Please enter the Sire of the Dam',
    },
    damdam: {
        type: String,
        trim: true,
        required: 'Please enter the Dam of the Dam',
    },
    siresiredam: {
        type: String,
        trim: true,
        index: true,
        required: 'Please enter the Sire of the Sire\'s Dam',
    },
    damsiredam: {
        type: String,
        trim: true,
        required: 'Please enter the Dam of the Sire\'s Dam',
    },
    siredamdam: {
        type: String,
        trim: true,
        required: 'Please enter the Sire of the Dam\'s Dam',
    },
    damdamdam: {
        type: String,
        trim: true,
        required: 'Please enter the Dam of the Dam\'s Dam',
    },
    file: {
        type: [String],
        trim: true,
        required: 'Please enter a picture',
    },
    fileOffspring: {
        type: [String],
        trim: true
    },
    terms: {
        type: String,
        trim: true,
        required: 'Please accept the terms & conditions',
    },
    labels: {
        type: String,
        trim: true,
    },
    online: {
        type: Boolean,
        default: 0
    },
    gallery: {
        type: Boolean,
        default: 0
    },
    ordersemen: {
        type: Boolean,
        default: 0
    },
    home: {
        type: Boolean,
        default: 0
    },
    owner: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: 'Every stallion needs an owner'
    } 
}, {
        toJSON: { virtuals: true},
        toObject: { virtuals: true},
});

// define our indexes
stallionSchema.index({
    birthname: 'text',
    sire: 'text',
    damsire: 'text'
});

stallionSchema.pre('save', async function(next) {
    if (!this.isModified('birthname')) {
        next();
        return;
    }
    this.slug = slug(this.birthname);
    const slugRegEx = new RegExp(`^(${this.slug})((-[0-9]*$)?)$`, 'i');
    const stallionsWithSlug = await this.constructor.find({ slug: slugRegEx });
    if (stallionsWithSlug.length) {
        this.slug = `${this.slug}-${stallionsWithSlug.length + 1}`;
    }
    next();
});

stallionSchema.statics.getTopStallions = function() {
    return this.aggregate([
        { $lookup: { from: 'reviews', localField: '_id', foreignField: 'stallion', as: 'reviews'} },
        { $unwind: "$reviews" },
       { $match: { 'reviews.0': { $exists: true }}},
        { $project: { 
            photo: '$$ROOT.file',
            slug: '$$ROOT.slug',
            reviews: '$$ROOT.reviews.author',
            birthname: '$$ROOT.birthname',
           // author: '$$ROOT.reviews.author',
            averageSizeRating: { $avg: '$reviews.rating_size'},
            averageModelRating: { $avg: '$reviews.model_size'},
            averageTypeRating: { $avg: '$reviews.type_size'},
            averageHeadRating: { $avg: '$reviews.head_size'},
            averageNeckRating: { $avg: '$reviews.neck_size'},
            averageOrientationRating: { $avg: '$reviews.orientation_size'},
            averageWalkLengthRating: { $avg: '$reviews.walklength_size'},
            averageWalkSuppleRating: { $avg: '$reviews.walksupple_size'},
            averageBraveRating: { $avg: '$reviews.brave_size'},
            averageBloodRating: { $avg: '$reviews.blood_size'},
            averageTrotLengthRating: { $avg: '$reviews.trotlength_size'},
            averageTrotSuppleRating: { $avg: '$reviews.trotsupple_size'},
            averageFrontRating: { $avg: '$reviews.front_size'},
            averageHindRating: { $avg: '$reviews.hind_size'},
            averageCanterLengthRating: { $avg: '$reviews.canterlength_size'},
            averageCanterSuppleRating: { $avg: '$reviews.cantersupple_size'},
            averageCarefulRating: { $avg: '$reviews.careful_size'},
            averageScopeRating: { $avg: '$reviews.scope_size'},
            averageBackRating: { $avg: '$reviews.back_size'},
            averageCharacterRating: { $avg: '$reviews.character_size'}}
        },
            { $project: { 
                photo: '$$ROOT.file',
                slug: '$$ROOT.slug',
                reviews: '$$ROOT.reviews[author]',
                birthname: '$$ROOT.birthname',
               // author: '$users.author',
                averageSizeRating: { $avg: '$reviews.rating_size'},
                averageModelRating: { $avg: '$reviews.model_size'},
                averageTypeRating: { $avg: '$reviews.type_size'},
                averageHeadRating: { $avg: '$reviews.head_size'},
                averageNeckRating: { $avg: '$reviews.neck_size'},
                averageOrientationRating: { $avg: '$reviews.orientation_size'},
                averageWalkLengthRating: { $avg: '$reviews.walklength_size'},
                averageWalkSuppleRating: { $avg: '$reviews.walksupple_size'},
                averageBraveRating: { $avg: '$reviews.brave_size'},
                averageBloodRating: { $avg: '$reviews.blood_size'},
                averageTrotLengthRating: { $avg: '$reviews.trotlength_size'},
                averageTrotSuppleRating: { $avg: '$reviews.trotsupple_size'},
                averageFrontRating: { $avg: '$reviews.front_size'},
                averageHindRating: { $avg: '$reviews.hind_size'},
                averageCanterLengthRating: { $avg: '$reviews.canterlength_size'},
                averageCanterSuppleRating: { $avg: '$reviews.cantersupple_size'},
                averageCarefulRating: { $avg: '$reviews.careful_size'},
                averageScopeRating: { $avg: '$reviews.scope_size'},
                averageBackRating: { $avg: '$reviews.back_size'},
                averageCharacterRating: { $avg: '$reviews.character_size'},
                averageTotalRating: { $avg: [ "$averageSizeRating", "$averageModelRating", "$averageTypeRating", "$averageHeadRating", "$averageNeckRating", "$averageOrientationRating", "$averageWalkLengthRating", "$averageWalkSuppleRating", "$averageBraveRating", "$averageBloodRating", "$averageTrotLengthRating", "$averageTrotSuppleRating", "$averageFrontRating", "$averageHindRating", "$averageCanterLengthRating", "$averageCanterSuppleRating", "$averageCarefulRating", "$averageScopeRating", "$averageBackRating", "$averageCharacterRating" ]}}
            },
        { $sort: { averageTotalRating: -1}},
        //limit to 10
        //{ $limit: 10 }
    ]);
};

stallionSchema.statics.getAverageStallion = function() {
    return this.aggregate([
        { $lookup: { from: 'reviews', localField: '_id', foreignField: 'stallion', as: 'reviews'} },
//        { $match: { 'reviews.1': { $exists: true }}},
        { $project: { 
            photo: '$$ROOT.file',
            stallion: '$reviews.stallion',
            author: '$reviews.author',
            slug: '$$ROOT.slug',
            reviews: '$$ROOT.reviews',
            birthname: '$$ROOT.birthname',
            averageSizeRating: { $avg: '$reviews.rating_size'},
            averageModelRating: { $avg: '$reviews.model_size'},
            averageTypeRating: { $avg: '$reviews.type_size'},
            averageHeadRating: { $avg: '$reviews.head_size'},
            averageNeckRating: { $avg: '$reviews.neck_size'},
            averageOrientationRating: { $avg: '$reviews.orientation_size'},
            averageWalkLengthRating: { $avg: '$reviews.walklength_size'},
            averageWalkSuppleRating: { $avg: '$reviews.walksupple_size'},
            averageBraveRating: { $avg: '$reviews.brave_size'},
            averageBloodRating: { $avg: '$reviews.blood_size'},
            averageTrotLengthRating: { $avg: '$reviews.trotlength_size'},
            averageTrotSuppleRating: { $avg: '$reviews.trotsupple_size'},
            averageFrontRating: { $avg: '$reviews.front_size'},
            averageHindRating: { $avg: '$reviews.hind_size'},
            averageCanterLengthRating: { $avg: '$reviews.canterlength_size'},
            averageCanterSuppleRating: { $avg: '$reviews.cantersupple_size'},
            averageCarefulRating: { $avg: '$reviews.careful_size'},
            averageScopeRating: { $avg: '$reviews.scope_size'},
            averageBackRating: { $avg: '$reviews.back_size'},
            averageCharacterRating: { $avg: '$reviews.character_size'}}
        },
            { $project: { 
                photo: '$$ROOT.file',
                stallion: '$$ROOT.stallion',
                author: '$$ROOT.author',
                slug: '$$ROOT.slug',
                reviews: '$$ROOT.reviews',
                birthname: '$$ROOT.birthname',
                averageSizeRating: { $avg: '$reviews.rating_size'},
                averageModelRating: { $avg: '$reviews.model_size'},
                averageTypeRating: { $avg: '$reviews.type_size'},
                averageHeadRating: { $avg: '$reviews.head_size'},
                averageNeckRating: { $avg: '$reviews.neck_size'},
                averageOrientationRating: { $avg: '$reviews.orientation_size'},
                averageWalkLengthRating: { $avg: '$reviews.walklength_size'},
                averageWalkSuppleRating: { $avg: '$reviews.walksupple_size'},
                averageBraveRating: { $avg: '$reviews.brave_size'},
                averageBloodRating: { $avg: '$reviews.blood_size'},
                averageTrotLengthRating: { $avg: '$reviews.trotlength_size'},
                averageTrotSuppleRating: { $avg: '$reviews.trotsupple_size'},
                averageFrontRating: { $avg: '$reviews.front_size'},
                averageHindRating: { $avg: '$reviews.hind_size'},
                averageCanterLengthRating: { $avg: '$reviews.canterlength_size'},
                averageCanterSuppleRating: { $avg: '$reviews.cantersupple_size'},
                averageCarefulRating: { $avg: '$reviews.careful_size'},
                averageScopeRating: { $avg: '$reviews.scope_size'},
                averageBackRating: { $avg: '$reviews.back_size'},
                averageCharacterRating: { $avg: '$reviews.character_size'},
                averageTotalRating: { $avg: [ "$averageSizeRating", "$averageModelRating", "$averageTypeRating", "$averageHeadRating", "$averageNeckRating", "$averageOrientationRating", "$averageWalkLengthRating", "$averageWalkSuppleRating", "$averageBraveRating", "$averageBloodRating", "$averageTrotLengthRating", "$averageTrotSuppleRating", "$averageFrontRating", "$averageHindRating", "$averageCanterLengthRating", "$averageCanterSuppleRating", "$averageCarefulRating", "$averageScopeRating", "$averageBackRating", "$averageCharacterRating" ]}}
            },
        { $sort: { averageTotalRating: -1}},
        //limit to 10
        //{ $limit: 10 }
    ]);
};

stallionSchema.virtual('reviews', {
    ref: 'Review', // what model to link?
    localField: '_id', //which field on the stallion?
    foreignField: 'stallion' //which field on the review?
});

stallionSchema.virtual('orders', {
    ref: 'Order', // what model to link?
    localField: '_id', //which field on the stallion?
    foreignField: 'stallion' //which field on the review?
});

stallionSchema.virtual('commentaar', {
    ref: 'Commentaar', // what model to link?
    localField: '_id', //which field on the stallion?
    foreignField: 'stallion' //which field on the review?
});

function autopopulate(next) {
    this.populate('reviews commentaar orders');
    next();
}

stallionSchema.pre('find', autopopulate);
stallionSchema.pre('findOne', autopopulate);

module.exports = mongoose.model('Stallion', stallionSchema);