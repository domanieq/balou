const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const slug = require('slugs');

const horseSchema = new mongoose.Schema({
    birthname: {
        type: String,
        trim: true,
        index: true
    },
    slug: String,
    sportname: {
        type: String,
        trim: true,
        index: true
    },
    height: {
        type: Number,
        trim: true,
        min: 1,
        max: 200
    },
    breeder: String,
    dateOfBirth: {
        type: Date,
        index: true,
    },
    updated: { 
        type: Date,
        default: Date.now 
    },
    color: {
        type: String,
        trim: true
    },
    gender: {
        type: String,
        trim: true,
        index: true,
    },
    level: {
        type: Number,
        trim: true,
        index: true,
    },
    studbook: {
        type: String,
        trim: true,
    },
    location: {
        type: {
            type: String,
            default: 'Point'
        },
        coordinates: [{
            type: Number,
         }],
        address: {
            type: String,
        }
    },
    lfg: {
        type: String,
        trim: true
    },
    wffs: {
        type: String,
        trim: true
    },
    category: [String],
    xxox: {
        type: Number,
        trim: true
    },
    approved: {
        type: [String],
        trim: true
    },
    sexe: {
        type: String,
        trim: true,
        index: true
    },
    available: {
        type: [String],
        trim: true
    },
    studfee: {
        type: Number
    },
    studfeestraw: {
        type: Number
    },
    semen: {
        type: [String],
        trim: true
    },
    semendescription: String,
    freshfrom: { 
        type: Date
    },
    freshuntil: { 
        type: Date
    },
    description: {
        type: String,
        trim: true,
        index: true
    },
    descriptionEN: {
        type: String,
        trim: true,
        index: true
    },
    result: {
        type: [String],
        trim: true,
    },
    youtube: [String],
    convertlink: [String],
    convertwee: String,
    converdrie: String,
    offspring: {
        type: [String],
        trim: true,
    },
    damline: {
        type: String,
        trim: true,
    },
    position: Number,
    feilink: {
        type: String,
        trim: true,
    },
    forsale: {
        type: String,
        trim: true,
    },
    homepage: {
        type: String,
        trim: true,
    },
    sire: {
        type: String,
        trim: true,
        index: true,
        required: 'Please enter a Sire',
    },
    dam: {
        type: String,
        trim: true,
        index: true,
        required: 'Please enter a Dam',
    },
    siresire: {
        type: String,
        trim: true,
        required: 'Please enter the Sire of the Sire',
    },
    damsire: {
        type: String,
        trim: true,
        index: true,
        required: 'Please enter the Dam of the Sire',
    },
    siresiresire: {
        type: String,
        trim: true,
        required: 'Please enter the Sire of the Sire\'s Sire',
    },
    damsiresire: {
        type: String,
        trim: true,
        required: 'Please enter the Dam of the Sire\'s Sire',
    },
    siredamsire: {
        type: String,
        trim: true,
        required: 'Please enter the Sire of the Dam\'s Sire',
    },
    damdamsire: {
        type: String,
        trim: true,
        required: 'Please enter the Dam of the Dam\'s Sire',
    },
    siredam: {
        type: String,
        trim: true,
        required: 'Please enter the Sire of the Dam',
    },
    damdam: {
        type: String,
        trim: true,
        required: 'Please enter the Dam of the Dam',
    },
    siresiredam: {
        type: String,
        trim: true,
        index: true,
        required: 'Please enter the Sire of the Sire\'s Dam',
    },
    damsiredam: {
        type: String,
        trim: true,
        required: 'Please enter the Dam of the Sire\'s Dam',
    },
    siredamdam: {
        type: String,
        trim: true,
        required: 'Please enter the Sire of the Dam\'s Dam',
    },
    damdamdam: {
        type: String,
        trim: true,
        required: 'Please enter the Dam of the Dam\'s Dam',
    },
    file: {
        type: [String],
        trim: true
    },
    updatedFile: {
        type: [String],
        trim: true
    },
    horselist: {
        type: [String],
        trim: true
    },
    offspringlist: {
        type: [String],
        trim: true
    },
    fileOffspring: {
        type: [String],
        trim: true
    },
    labels: {
        type: String,
        trim: true,
    },
    draagmoeder: {
        type: String,
        trim: true,
    },
    online: {
        type: Boolean,
        default: 0
    },
    gallery: {
        type: Boolean,
        default: 0
    },
    ordersemen: {
        type: Boolean,
        default: 0
    },
    home: {
        type: Boolean,
        default: 0
    },
    owner: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: 'Every horse needs an owner'
    } 
}, {
        toJSON: { virtuals: true},
        toObject: { virtuals: true},
});

// define our indexes
horseSchema.index({
    birthname: 'text',
    sportname: 'text',
    sire: 'text',
    damsire: 'text',
    description: 'text',
    descriptionEN: 'text'},
    { name: 'search_fields' }
);

horseSchema.pre('save', async function(next) {
    if (!this.isModified('birthname')) {
        next();
        return;
    }
    this.slug = slug(this.birthname);
    const slugRegEx = new RegExp(`^(${this.slug})((-[0-9]*$)?)$`, 'i');
    const horsesWithSlug = await this.constructor.find({ slug: slugRegEx });
    if (horsesWithSlug.length) {
        this.slug = `${this.slug}-${horsesWithSlug.length + 1}`;
    }
    const url= this.youtube[0];
    function getId(url) {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        var match = url.match(regExp);
    
        if (match && match[2].length == 11) {
            return match[2];
        } else {
            console.log("url:" + url);
            console.log('match' + match);
        }
    };
    this.convertlink = getId(url);

    if(this.youtube[1]!==""){
    const urltwee=this.youtube[1];
    function getId(urltwee) {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        var match = urltwee.match(regExp);
    
        if (match && match[2].length == 11) {
            return match[2];
        } else {
            return 'error';
        }
    };
    const convertwee = getId(urltwee);
    this.convertlink.push(convertwee);
    }
    // cleanup empty values from sensitive arrays
    cleanupArrays(this);
    next();
});

horseSchema.pre('findOneAndUpdate', function (next) {
    // cleanup empty values from sensitive arrays
    const doc = this.getUpdate();
    cleanupArrays(doc);
    next();
});

horseSchema.statics.getTopHorses = function() {
    return this.aggregate([
        { $lookup: { from: 'reviews', localField: '_id', foreignField: 'horse', as: 'reviews'} },
        { $unwind: "$reviews" },
       { $match: { 'reviews.0': { $exists: true }}},
        { $project: { 
            photo: '$$ROOT.file',
            slug: '$$ROOT.slug',
            reviews: '$$ROOT.reviews.author',
            birthname: '$$ROOT.birthname',
           // author: '$$ROOT.reviews.author',
            averageSizeRating: { $avg: '$reviews.rating_size'},
            averageModelRating: { $avg: '$reviews.model_size'},
            averageTypeRating: { $avg: '$reviews.type_size'},
            averageHeadRating: { $avg: '$reviews.head_size'},
            averageNeckRating: { $avg: '$reviews.neck_size'},
            averageOrientationRating: { $avg: '$reviews.orientation_size'},
            averageWalkLengthRating: { $avg: '$reviews.walklength_size'},
            averageWalkSuppleRating: { $avg: '$reviews.walksupple_size'},
            averageBraveRating: { $avg: '$reviews.brave_size'},
            averageBloodRating: { $avg: '$reviews.blood_size'},
            averageTrotLengthRating: { $avg: '$reviews.trotlength_size'},
            averageTrotSuppleRating: { $avg: '$reviews.trotsupple_size'},
            averageFrontRating: { $avg: '$reviews.front_size'},
            averageHindRating: { $avg: '$reviews.hind_size'},
            averageCanterLengthRating: { $avg: '$reviews.canterlength_size'},
            averageCanterSuppleRating: { $avg: '$reviews.cantersupple_size'},
            averageCarefulRating: { $avg: '$reviews.careful_size'},
            averageScopeRating: { $avg: '$reviews.scope_size'},
            averageBackRating: { $avg: '$reviews.back_size'},
            averageCharacterRating: { $avg: '$reviews.character_size'}}
        },
            { $project: { 
                photo: '$$ROOT.file',
                slug: '$$ROOT.slug',
                reviews: '$$ROOT.reviews[author]',
                birthname: '$$ROOT.birthname',
               // author: '$users.author',
                averageSizeRating: { $avg: '$reviews.rating_size'},
                averageModelRating: { $avg: '$reviews.model_size'},
                averageTypeRating: { $avg: '$reviews.type_size'},
                averageHeadRating: { $avg: '$reviews.head_size'},
                averageNeckRating: { $avg: '$reviews.neck_size'},
                averageOrientationRating: { $avg: '$reviews.orientation_size'},
                averageWalkLengthRating: { $avg: '$reviews.walklength_size'},
                averageWalkSuppleRating: { $avg: '$reviews.walksupple_size'},
                averageBraveRating: { $avg: '$reviews.brave_size'},
                averageBloodRating: { $avg: '$reviews.blood_size'},
                averageTrotLengthRating: { $avg: '$reviews.trotlength_size'},
                averageTrotSuppleRating: { $avg: '$reviews.trotsupple_size'},
                averageFrontRating: { $avg: '$reviews.front_size'},
                averageHindRating: { $avg: '$reviews.hind_size'},
                averageCanterLengthRating: { $avg: '$reviews.canterlength_size'},
                averageCanterSuppleRating: { $avg: '$reviews.cantersupple_size'},
                averageCarefulRating: { $avg: '$reviews.careful_size'},
                averageScopeRating: { $avg: '$reviews.scope_size'},
                averageBackRating: { $avg: '$reviews.back_size'},
                averageCharacterRating: { $avg: '$reviews.character_size'},
                averageTotalRating: { $avg: [ "$averageSizeRating", "$averageModelRating", "$averageTypeRating", "$averageHeadRating", "$averageNeckRating", "$averageOrientationRating", "$averageWalkLengthRating", "$averageWalkSuppleRating", "$averageBraveRating", "$averageBloodRating", "$averageTrotLengthRating", "$averageTrotSuppleRating", "$averageFrontRating", "$averageHindRating", "$averageCanterLengthRating", "$averageCanterSuppleRating", "$averageCarefulRating", "$averageScopeRating", "$averageBackRating", "$averageCharacterRating" ]}}
            },
        { $sort: { averageTotalRating: -1}},
        //limit to 10
        //{ $limit: 10 }
    ]);
};

horseSchema.statics.getAverageHorse = function() {
    return this.aggregate([
        { $lookup: { from: 'reviews', localField: '_id', foreignField: 'horse', as: 'reviews'} },
//        { $match: { 'reviews.1': { $exists: true }}},
        { $project: { 
            photo: '$$ROOT.file',
            horse: '$reviews.horse',
            author: '$reviews.author',
            slug: '$$ROOT.slug',
            reviews: '$$ROOT.reviews',
            birthname: '$$ROOT.birthname',
            averageSizeRating: { $avg: '$reviews.rating_size'},
            averageModelRating: { $avg: '$reviews.model_size'},
            averageTypeRating: { $avg: '$reviews.type_size'},
            averageHeadRating: { $avg: '$reviews.head_size'},
            averageNeckRating: { $avg: '$reviews.neck_size'},
            averageOrientationRating: { $avg: '$reviews.orientation_size'},
            averageWalkLengthRating: { $avg: '$reviews.walklength_size'},
            averageWalkSuppleRating: { $avg: '$reviews.walksupple_size'},
            averageBraveRating: { $avg: '$reviews.brave_size'},
            averageBloodRating: { $avg: '$reviews.blood_size'},
            averageTrotLengthRating: { $avg: '$reviews.trotlength_size'},
            averageTrotSuppleRating: { $avg: '$reviews.trotsupple_size'},
            averageFrontRating: { $avg: '$reviews.front_size'},
            averageHindRating: { $avg: '$reviews.hind_size'},
            averageCanterLengthRating: { $avg: '$reviews.canterlength_size'},
            averageCanterSuppleRating: { $avg: '$reviews.cantersupple_size'},
            averageCarefulRating: { $avg: '$reviews.careful_size'},
            averageScopeRating: { $avg: '$reviews.scope_size'},
            averageBackRating: { $avg: '$reviews.back_size'},
            averageCharacterRating: { $avg: '$reviews.character_size'}}
        },
            { $project: { 
                photo: '$$ROOT.file',
                horse: '$$ROOT.horse',
                author: '$$ROOT.author',
                slug: '$$ROOT.slug',
                reviews: '$$ROOT.reviews',
                birthname: '$$ROOT.birthname',
                averageSizeRating: { $avg: '$reviews.rating_size'},
                averageModelRating: { $avg: '$reviews.model_size'},
                averageTypeRating: { $avg: '$reviews.type_size'},
                averageHeadRating: { $avg: '$reviews.head_size'},
                averageNeckRating: { $avg: '$reviews.neck_size'},
                averageOrientationRating: { $avg: '$reviews.orientation_size'},
                averageWalkLengthRating: { $avg: '$reviews.walklength_size'},
                averageWalkSuppleRating: { $avg: '$reviews.walksupple_size'},
                averageBraveRating: { $avg: '$reviews.brave_size'},
                averageBloodRating: { $avg: '$reviews.blood_size'},
                averageTrotLengthRating: { $avg: '$reviews.trotlength_size'},
                averageTrotSuppleRating: { $avg: '$reviews.trotsupple_size'},
                averageFrontRating: { $avg: '$reviews.front_size'},
                averageHindRating: { $avg: '$reviews.hind_size'},
                averageCanterLengthRating: { $avg: '$reviews.canterlength_size'},
                averageCanterSuppleRating: { $avg: '$reviews.cantersupple_size'},
                averageCarefulRating: { $avg: '$reviews.careful_size'},
                averageScopeRating: { $avg: '$reviews.scope_size'},
                averageBackRating: { $avg: '$reviews.back_size'},
                averageCharacterRating: { $avg: '$reviews.character_size'},
                averageTotalRating: { $avg: [ "$averageSizeRating", "$averageModelRating", "$averageTypeRating", "$averageHeadRating", "$averageNeckRating", "$averageOrientationRating", "$averageWalkLengthRating", "$averageWalkSuppleRating", "$averageBraveRating", "$averageBloodRating", "$averageTrotLengthRating", "$averageTrotSuppleRating", "$averageFrontRating", "$averageHindRating", "$averageCanterLengthRating", "$averageCanterSuppleRating", "$averageCarefulRating", "$averageScopeRating", "$averageBackRating", "$averageCharacterRating" ]}}
            },
        { $sort: { averageTotalRating: -1}},
        //limit to 10
        //{ $limit: 10 }
    ]);
};

horseSchema.virtual('reviews', {
    ref: 'Review', // what model to link?
    localField: '_id', //which field on the horse?
    foreignField: 'horse' //which field on the review?
});

horseSchema.virtual('orders', {
    ref: 'Order', // what model to link?
    localField: '_id', //which field on the horse?
    foreignField: 'horse' //which field on the review?
});

horseSchema.virtual('commentaar', {
    ref: 'Commentaar', // what model to link?
    localField: '_id', //which field on the horse?
    foreignField: 'horse' //which field on the review?
});

function autopopulate(next) {
    this.populate('reviews commentaar orders');
    next();
}

function cleanupArrays(doc) { // doc will be modified by link. No need to return
    
    if (Array.isArray(doc.result) && doc.result.length > 0) {
        doc.result = doc.result.filter(e => e);
    }

    if (Array.isArray(doc.youtube) && doc.youtube.length > 0) {
        doc.youtube = doc.youtube.filter(e => e);
    }

    if (Array.isArray(doc.offspring) && doc.offspring.length > 0) {
        doc.offspring = doc.offspring.filter(e => e);
    }

    if (Array.isArray(doc.fileOffspring) && doc.fileOffspring.length > 0) {
        doc.fileOffspring = doc.fileOffspring.filter(e => e);
    }

    if (Array.isArray(doc.file) && doc.file.length > 0) {
        doc.file = doc.file.filter(e => e);
    }

    if (Array.isArray(doc.available) && doc.available.length > 0) {
        doc.available = doc.available.filter(e => e);
    }
}

horseSchema.pre('find', autopopulate);
horseSchema.pre('findOne', autopopulate);

module.exports = mongoose.model('Horse', horseSchema);