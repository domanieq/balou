const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const reviewSchema = new mongoose.Schema({
    created: {
        type: Date,
        default: Date.now
    },
    author: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: 'You must supply an author!'
    },
    stallion: {
        type: mongoose.Schema.ObjectId,
        ref: 'Stallion',
        required: 'You must supply a stallion'
    },
    rating_size: {
        type: Number,
        min: 1,
        max:5
    },
    model_size: {
        type: Number,
        min: 1,
        max:5
    },
    type_size: {
        type: Number,
        min: 1,
        max:5
    },
    head_size: {
        type: Number,
        min: 1,
        max:5
    },
    neck_size: {
        type: Number,
        min: 1,
        max:5
    },
    orientation_size: {
        type: Number,
        min: 1,
        max:5
    },
    walklength_size: {
        type: Number,
        min: 1,
        max:5
    },
    walksupple_size: {
        type: Number,
        min: 1,
        max:5
    },
    brave_size: {
        type: Number,
        min: 1,
        max:5
    },
    blood_size: {
        type: Number,
        min: 1,
        max:5
    },
    trotlength_size: {
        type: Number,
        min: 1,
        max:5
    },
    trotsupple_size: {
        type: Number,
        min: 1,
        max:5
    },
    front_size: {
        type: Number,
        min: 1,
        max:5
    },
    hind_size: {
        type: Number,
        min: 1,
        max:5
    },
    canterlength_size: {
        type: Number,
        min: 1,
        max:5
    },
    cantersupple_size: {
        type: Number,
        min: 1,
        max:5
    },
    careful_size: {
        type: Number,
        min: 1,
        max:5
    },
    scope_size: {
        type: Number,
        min: 1,
        max:5
    },
    back_size: {
        type: Number,
        min: 1,
        max:5
    },
    character_size: {
        type: Number,
        min: 1,
        max:5
    },
    comment: String
});

function autopopulate(next) {
    this.populate('author');
    next();
}

reviewSchema.pre('find', autopopulate);
reviewSchema.pre('findOne', autopopulate);

module.exports = mongoose.model('Review', reviewSchema);