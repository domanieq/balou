const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const slug = require('slugs');

// var hoursSchema = new mongoose.Schema({
//     weekday: [{
//         type: Number,
//         required: 'Please enter the location of the horse',
//         starthour: {
//                 type: Number
//             },
//         endhour: {
//                 type: Number
//             }
//          }]
// }, {
//     toJSON: { virtuals: true},
//     toObject: { virtuals: true},
// });

const vetSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        required: 'Please enter a Name',
    },
    slug: String,
    phone1: {
        type: String,
        trim: true,
        required: 'Please enter a Phonenumber',
    },
    phone2: {
        type: String,
        trim: true,
    },
    fax: {
        type: String,
    },
    updated: { 
        type: Date,
        default: Date.now 
    },
    email: {
        type: String,
        trim: true,
        required: 'Please enter an Email Address',
    },
    location: {
        type: {
            type: String,
            default: 'Point'
        },
        coordinates: [{
            type: Number,
            required: 'Please enter the location'
         }],
        address: {
            type: String,
            required: 'You must supply an address!'
        }
    },
    services: {
        type: [String],
        required: 'You must have services!'
    },
    website: {
        type: String,
        trim: true
    },
    prices: {
        type: String,
        trim: true,
    },
    hours: {
            day: [{
                type: String,
            }],
            starthour: [{
                type: Number,
                required: 'Please enter at least one starthour'
             }],
            endhour: [{
                type: Number,
                required: 'You must supply an endhour!'
            }]
    },
    description: {
        type: String,
        trim: true,
        required: 'Please enter a Description',
    },
    file: {
        type: [String],
        trim: true,
        required: 'Please enter a picture',
    },
    terms: {
        type: String,
        trim: true,
        required: 'Please accept the terms & conditions',
    },
    online: Boolean,
    gallery: Boolean,
    owner: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: 'Every breeding center needs an owner'
    },
    stallionlist: {
        type: [String]
    }
}, {
    toJSON: { virtuals: true},
    toObject: { virtuals: true},
});


// define our indexes
vetSchema.index({
    name: 'text',
    services: 'text',
});

vetSchema.index({ location: '2dsphere' });

vetSchema.pre('save', async function(next) {
    if (!this.isModified('name')) {
        next();
        return;
    }
    this.slug = slug(this.name);
    const slugRegEx = new RegExp(`^(${this.slug})((-[0-9]*$)?)$`, 'i');
    const vetsWithSlug = await this.constructor.find({ slug: slugRegEx });
    if (vetsWithSlug.length) {
        this.slug = `${this.slug}-${vetsWithSlug.length + 1}`;
    }
    next();
});

vetSchema.statics.getServicesList = function() {
    return this.aggregate([
        { $unwind: '$services' },
        { $group: { _id: '$services', count: { $sum:1}}},
        { $sort: { count: -1}}
    ]);
};

vetSchema.virtual('commentaarVet', {
    ref: 'CommentaarVet',
    localField: '_id',
    foreignField: 'vet'
});

function autopopulate(next) {
    this.populate('commentaarVet');
    next();
}

vetSchema.pre('find', autopopulate);
vetSchema.pre('findOne', autopopulate);


module.exports = mongoose.model('Vet', vetSchema);