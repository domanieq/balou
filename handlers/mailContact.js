const nodemailer = require('nodemailer');
const pug = require('pug');
const juice = require('juice');
const htmlToText = require('html-to-text');
const promisify = require('es6-promisify');

var transport = nodemailer.createTransport({
    host: "smtp-auth.mailprotect.be",
    port: 465,
    auth: {
      user: "info@baloudureventon.com",
      pass: "B@lou1R3v3nton"
    }
  });

const generateHTML = (filename, options = {}) => {
  const html = pug.renderFile(`${__dirname}/../views/email/${filename}.pug`, options);
  const inlined = juice(html);
  return inlined;
};

exports.send = async (options) => {
  const html = generateHTML(options.filename, options);
  const text = htmlToText.fromString(html);

  const mailOptions = {
    sender: options.sender,
    from: options.email,
    message: options.message,
    to: `info@baloudureventon.com, ${options.email}`,
    subject: options.subject,
    html,
    text
  };
  const sendMail = promisify(transport.sendMail, transport);
  return sendMail(mailOptions);
};
