const nodemailer = require('nodemailer');
const pug = require('pug');
const juice = require('juice');
const htmlToText = require('html-to-text');
const promisify = require('es6-promisify');

var transport = nodemailer.createTransport({
    host: "smtp-auth.mailprotect.be",
    port: 465,
    auth: {
      user: "info@jumpingstallions.com",
      pass: "91CnM06mama"
    }
  });

const generateHTML = (filename, options = {}) => {
  const html = pug.renderFile(`${__dirname}/../views/email/${filename}.pug`, options);
  const inlined = juice(html);
  return inlined;
};

exports.send = async (options) => {
  const html = generateHTML(options.filename, options);
  const text = htmlToText.fromString(html);

  const mailOptions = {
    sender: options.sender,
    from: options.email,
    message: options.message,
    to: `info@jumpingstallions.com, ${options.email}, ${options.stallionemail}`,
    subject: options.subject,
    stallionname: options.stallionname,
    sementype: options.sementype,
    marename: options.marename,
    UELN: options.UELN,
    dateOfBirthMare: options.dateOfBirthMare,
    studbookmare: options.studbookmare,
    siremare: options.siremare,
    damsiremare: options.damsiremare,
    file: options.file,
    insemination: options.insemination,
    previous: options.previous,
    etoption: options.etoption,
    breedingass: options.breedingass,
    membershipnr: options.membershipnr,
    delivery: options.delivery,
    deliveryDate: options.deliveryDate,
    shipoption: options.shipoption,
    locationother: options.locationother.address,
    locationowner: options.locationowner.address,
    VAT: options.VAT,
    phoneowner1: options.phoneowner1,
    phoneowner2: options.phoneowner2,
    namevet: options.namevet,
    locationvet: options.locationvet.address,
    phonevet1: options.phonevet1,
    phonevet2: options.phonevet2,
    emailvet: options.emailvet,
    comments: options.comments,
    html,
    text
  };
  const sendMail = promisify(transport.sendMail, transport);
  return sendMail(mailOptions);
};
