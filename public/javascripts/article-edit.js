/* ========================================================================
 * articleEdit
 * Apply handlers for image removal process. Can be extended with another js functions.
 * 
 * Init example:
 * $('.js-article-edit-form').articleEdit({
 *     
 * });
 * ======================================================================== */

(function ($, window, document) {

    var pluginName = 'articleEdit';
    var _this;

    var Plugin = function (element, options) {
        _this = this;
        _this.$element = $(element);

        _this.options = $.extend({
            // options here
        }, options);

        _this.init();
    };

    Plugin.prototype.init = function () {
        _this.applyImageRemoveListener();
    };

    Plugin.prototype.applyImageRemoveListener = function () {

        console.log(_this.$element.find('.js-remove-image'));

        // collect text and date inputs name and value
        _this.$element.find('.js-remove-image').on('click', function (e) {
            e.preventDefault();

            var $this = $(this);
            var articleId = $this.attr('data-article-id');
            var type = $this.attr('data-file-type');
            var type2 = $this.attr('data-file-type2');
            var fileName = $this.attr('data-file-name');
            var message = $this.attr('data-message');

            // ask for confirmation
            if (window.confirm(message)) {
                // send ajax
                _this.sendForm({ articleId, type, type2, fileName }, function () {
                    // if successfull delete, remove block with image
                    $this.parent().remove();
                });
            }
        });
    }
    Plugin.prototype.sendForm = function (params, successCallback) {

        // send search query
        $.ajax({
            url: `/api/news/${params.articleId}/removeFile/${params.type}/${params.type2}/${params.fileName}`,
            method: 'DELETE',
            success: function (data) {
                if (data && data.success) {
                    // remove the image from DOM
                    successCallback();
                }
                else {
                    console.log('Uexpected error.');
                }
            }
        }).fail(function (xhr, textStatus) {

            const responseJSON = xhr.responseJSON;
            // if existing record ask for replace
            if (responseJSON && responseJSON.error) {
                window.alert(responseJSON.error);
            }
            else {
                window.alert(textStatus);
            }
        });
    }

    $.fn[pluginName] = function (options) {
        return this.each(function () {
            $.data(this, pluginName, new Plugin(this, options));
        });
    };
})(jQuery, window, document);
