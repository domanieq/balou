import '../sass/style.scss';

import { $, $$ } from './modules/bling';
import autocomplete from './modules/autocomplete';
import typeAhead from './modules/typeAhead';
import typeNews from './modules/typeNews';
import typeVets from './modules/typeVets';
import makeMap from './modules/map';
import ajaxHeart from './modules/heart';

autocomplete( $('#address'), $('#lat'), $('#lng') );
autocomplete( $('#addressowner'), $('#latowner'), $('#lngowner') );
autocomplete( $('#addressvet'), $('#latvet'), $('#lngvet') );

makeMap( $('#map'));

const heartForms = $$('form.heart');
heartForms.on('submit', ajaxHeart);
const test = $$('selectmare');
console.log(test);

typeAhead( $('.search-ahead'));
typeHorse( $('.search'));
typeNews( $('.search-news'));
typeVets( $('.search-vets'));
