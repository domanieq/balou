/* ========================================================================
 * findSire
 * Apply handlers for finding a sire in the database. Can be extended with another js functions.
 * 
 * Init example:
 * $('.js-find-sire-form').findSire({
 *     
 * });
 * ======================================================================== */

(function ($, window, document) {

    var pluginName = 'findSire';
    var _this;

    var Plugin = function (element, options) {
        _this = this;
        _this.$element = $(element);

        _this.options = $.extend({
            // options here
        }, options);

        _this.init();
    };

    Plugin.prototype.init = function () {
        _this.applySireListener();
    };

    Plugin.prototype.applySireListener = function () {
        var $input = _this.$element.find('.js-sire-name');
        var options = {
            url: '/api/sires/autocomplete',
            getValue: 'birthname',
            ajaxSettings: {
                dataType: 'json',
                method: 'GET',
                data: {}
            },
            preparePostData: function (data) {
                data.phrase = $input.val();
                return data;
            },
            list: {
                onChooseEvent: function () {
                    var selectedItem = $input.getSelectedItemData();
                    _this.fillVetForm(selectedItem);
                }
            },
            requestDelay: 400
        };
        $input.easyAutocomplete(options);
    }

    Plugin.prototype.fillVetForm = function (data) {

        _this.$element.find('input[name="siresire"]').val(data.sire);
        _this.$element.find('input[name="siredam"]').val(data.dam);
        _this.$element.find('input[name="siresiresire"]').val(data.siresire);
        _this.$element.find('input[name="siresiredam"]').val(data.siredam);
        _this.$element.find('input[name="siredamsire"]').val(data.damsire);
        _this.$element.find('input[name="siredamdam"]').val(data.damdam);
    }


    // Plugin.prototype.getMareDetails = function (mareId, successCallback) {

    //     // send search query
    //     $.ajax({
    //         url: '/api/mares/' + mareId,
    //         method: 'GET',
    //         success: function (data) {
    //             if (data && data._id) {
    //                 // return data in callback
    //                 successCallback(data);
    //             }
    //             else {
    //                 console.log('Uexpected error.');
    //             }
    //         }
    //     }).fail(function (xhr, textStatus) {

    //         const responseJSON = xhr.responseJSON;
    //         // if existing record ask for replace
    //         if (responseJSON && responseJSON.error) {
    //             window.alert(responseJSON.error);
    //         }
    //         else {
    //             window.alert(textStatus);
    //         }
    //     });
    // }

    $.fn[pluginName] = function (options) {
        return this.each(function () {
            $.data(this, pluginName, new Plugin(this, options));
        });
    };
})(jQuery, window, document);
