import axios from 'axios';
import { $ } from './bling';

const mapOptions = {
  center: { lat: 51.18, lng: 5.51 },
  zoom: 2
};

function loadPlaces(map, lat = 51.18, lng = 5.51, radius = 30000) {
  const radiusinput = $('[name="radius"]');
  radiusinput.on('input', function() {
    if(!this.value){
      return;
    }
    
    const radius = this.value;
  axios.get(`/api/vets/near?lat=${lat}&lng=${lng}&maxDistance=${radius}`)
    .then(res => {
      const places = res.data;
      if (!places.length) {
        alert('no places found!');
        return;
      }

      const bounds = new google.maps.LatLngBounds();
      const infoWindow = new google.maps.InfoWindow();

      const markers = places.map(place => {
        const [placeLng, placeLat] = place.location.coordinates;
        const position = { lat: placeLat, lng: placeLng };
        bounds.extend(position);
        const marker = new google.maps.Marker({ map, position });
        marker.place = place;
        return marker;
      });

      // when someone clicks on a marker, show the details of that place
      markers.forEach(marker => marker.addListener('click', function() {
        console.log(this.place);
        const html = `
          <div class="popup">
            <a href="/centers/${this.place.slug}">
              <img class="maps-img" src="/uploads/${this.place.file || 'store.png'}" alt="${this.place.name}" />
              <p style="font-size:15px;font-weight:400;">${this.place.name}</p>
              <p>${this.place.location.address}</p>
            </a>
          </div>
        `;
        infoWindow.setContent(html);
        infoWindow.open(map, this);
      }));

      // then zoom the map to fit all the markers perfectly
      map.setCenter(bounds.getCenter());
      map.fitBounds(bounds);
 })
});
axios.get(`/api/vets/near?lat=${lat}&lng=${lng}&maxDistance=${radius}`)
.then(res => {
  const places = res.data;
  if (!places.length) {
    alert('no places found!');
    return;
  }

  const bounds = new google.maps.LatLngBounds();
  const infoWindow = new google.maps.InfoWindow();

  const markers = places.map(place => {
    const [placeLng, placeLat] = place.location.coordinates;
    const position = { lat: placeLat, lng: placeLng };
    bounds.extend(position);
    const marker = new google.maps.Marker({ map, position });
    marker.place = place;
    return marker;
  });

  // when someone clicks on a marker, show the details of that place
  markers.forEach(marker => marker.addListener('click', function() {
    console.log(this.place);
    const html = `
      <div class="popup">
        <a href="/centers/${this.place.slug}">
          <img class="maps-img" src="/uploads/${this.place.file || 'store.png'}" alt="${this.place.name}" />
          <p style="font-size:15px;font-weight:400;">${this.place.name}</p>
          <p>${this.place.location.address}</p>
        </a>
      </div>
    `;
    infoWindow.setContent(html);
    infoWindow.open(map, this);
  }));

  // then zoom the map to fit all the markers perfectly
  map.setCenter(bounds.getCenter());
  map.fitBounds(bounds);
})
};



function makeMap(mapDiv) {
  if (!mapDiv) return;
  // make our map
  const map = new google.maps.Map(mapDiv, mapOptions);
  loadPlaces(map);

  const input = $('[name="geolocate"]');
  // const radiusinput = $('[name="radius"]');
  // radiusinput.on('input', function() {
  //   if(!this.value){
  //     return;
  //   }

  //   const radius = this.value;
  //   const lat = 51.18;
  //   const lng = 5.51;
  //   axios.get(`/api/vets/near?lat=${lat}&lng=${lng}&maxDistance=${radius}`)
  //   .then(res => {
  //     console.log(res.data);
  //     });
  //   console.log(radius);
  // });
  
  const autocomplete = new google.maps.places.Autocomplete(input);
  autocomplete.addListener('place_changed', () => {
    const place = autocomplete.getPlace();
    loadPlaces(map, place.geometry.location.lat(), place.geometry.location.lng());
  });
}

export default makeMap;
