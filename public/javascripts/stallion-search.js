/* ========================================================================
 * Search Form
 * Provides real-time filtering for the Stallions page
 * 
 * Init example:
 * $('.js-search-form').stallionSearch({
 *     $template: $('#stallion-card'), // Pass jQuery selector here
 *     $container: $('#stallion-container'),
 *     $pagination: $('#stallion-pagination')
 * });
 * ======================================================================== */

(function ($, wNumb, window, document) {

    var pluginName = 'stallionSearch';
    var _this;
    var format = {
        money: wNumb({
            decimals: 3,
            thousand: '.',
            prefix: '€ '
        }),
        height: wNumb({
            decimals: 0,
            hundred: '.',
            suffix: ' cm',
        }),
        level: wNumb({
            decimals: 2,
            thousand: ',',
            suffix: ' m',
        }),
        age: wNumb({
            decimals: 0,
            thousand: ',',
            suffix: ' years',
        })
    }

    var Plugin = function (element, options) {

        _this = this;

        _this.$element = $(element);
        _this.options = $.extend({
            $template: $('#stallion-card'),
            $container: $('#stallion-container'),
            $pagination: $('#stallion-pagination')
        }, options);

        // get HTML content from the template
        _this.template = this.options.$template.html();
        _this.$container = this.options.$container;
        _this.$pagination = this.options.$pagination;

        // detect initial page
        var pathArray = window.location.pathname.split('/');
        if (pathArray.indexOf('page') !== -1) {
            _this.page = pathArray.pop(); // take the last page
        }
        else {
            _this.page = 1; // initial page
        }
        _this.limit = 12;

        _this.init();
    };

    var throttled = function throttled(delay, fn) {
        var lastCall = 0;
        return function (arguments) {
            var now = (new Date).getTime();
            if (now - lastCall < delay) {
                return;
            }
            lastCall = now;
            return fn(arguments);
        }
    }

    Plugin.prototype.init = function () {

        _this.applyTextListeners();
        _this.applyCheckboxListeners();
        _this.applyPaginationListeners();
        _this.initSliders();
    };

    Plugin.prototype.collectData = function () {

        var data = {};

        // collect text inputs name and value
        _this.$element.find('input[type="text"]').each(function () {
            var $this = $(this);
            var name = $this.attr('name');
            var val = $this.val();

            if (name && val) {
                data[name] = val;
            }
        });

        // collect text inputs date
        _this.$element.find('input[type="date"]').each(function () {
            var $this = $(this);
            var date = $this.attr('date');
            var val = $this.val();
        
                    if (name && val) {
                        data[date] = val;
                    }
        });

        // collect checkbox name and value
        _this.$element.find('input[type="checkbox"]').each(function () {
            var $this = $(this);
            if ($this.is(':checked')) {
                var name = $this.attr('name');
                var val = $this.val();

                if (Array.isArray(data[name])) {
                    // add new value
                    data[name].push(val);
                }
                else {
                    // create an array with first value
                    data[name] = [val];
                }
            }
        });

        // collect radio inputs name and value
        _this.$element.find('input[type="radio"]').each(function () {
            var $this = $(this);
            if ($this.is(':checked')) {
                data[$this.attr('name')] = $this.val();
            }
        });

        var priceSlider = document.getElementById('price-slider');
        var priceRange = priceSlider.noUiSlider.get();
        data.priceRange = priceRange.map(format.money.from);

        var heightSlider = document.getElementById('height-slider');
        var heightRange = heightSlider.noUiSlider.get();
        data.heightRange = heightRange.map(format.height.from);

        var levelSlider = document.getElementById('level-slider');
        var levelRange = levelSlider.noUiSlider.get();
        data.levelRange = levelRange.map(format.level.from);

        var ageSlider = document.getElementById('age-slider');
        var ageRange = ageSlider.noUiSlider.get();
        data.ageRange = ageRange.map(format.age.from);

        data.page = _this.page;
        data.limit = _this.limit;

        return data;
    }

    Plugin.prototype.applyTextListeners = function () {
        // Listen for input modifications and throttle it
        _this.$element.find('input[type="text"]').on('keyup', throttled(200, _this.search.bind(_this)));
    }

    Plugin.prototype.applyDateListeners = function () {
        // Listen for input modifications and throttle it
        _this.$element.find('input[type="date"]').on('keyup', throttled(200, _this.search.bind(_this)));
    }

    Plugin.prototype.applyCheckboxListeners = function () {
        // Listen for checkboxes and radio
        _this.$element.find('input[type="checkbox"], input[type="radio"]').on('change', _this.search.bind(_this));
    }

    Plugin.prototype.applyPaginationListeners = function () {
        // Listen for Next and Prev links
        _this.$pagination.find('a').on('click', function (e) {
            e.preventDefault();
            if ($(this).hasClass('next-page')) {
                _this.page += 1;
            }
            else {
                _this.page -= 1;
            }

            // change url
            window.history.pushState(null, null, '/stallions/page/' + _this.page);

            // trigger search
            _this.search();
        });
    }

    Plugin.prototype.initSliders = function () {

        // init price slider
        var priceSlider = document.getElementById('price-slider');
        noUiSlider.create(priceSlider, {
            // start: [500, 2000],
            start: [0, 4000],
            tooltips: true,
            step: 50,
            connect: true,
            range: {
                min: 0,
                max: 4000
            },
            format: format.money
        });

        priceSlider.noUiSlider.on('change', _this.search.bind(_this));

        // init height slider
        var heightSlider = document.getElementById('height-slider');
        noUiSlider.create(heightSlider, {
            // start: [160, 180],
            start: [140, 190],
            tooltips: true,
            step: 1,
            connect: true,
            range: {
                min: 140,
                max: 190
            },
            format: format.height
        });

        heightSlider.noUiSlider.on('change', _this.search.bind(_this));

        // init level slider
        var levelSlider = document.getElementById('level-slider');
        noUiSlider.create(levelSlider, {
            // start: [1.2, 1.45],
            start: [1, 1.6],
            step: 0.05,
            tooltips: true,
            connect: true,
            range: {
                min: 1,
                max: 1.6
            },
            format: format.level
        });

        levelSlider.noUiSlider.on('change', _this.search.bind(_this));

        // init age slider
        var ageSlider = document.getElementById('age-slider');
        noUiSlider.create(ageSlider, {
            // start: [7, 15],
            start: [0, 30],
            step: 1,
            tooltips: true,
            connect: true,
            range: {
                min: 0,
                max: 30
            },
            format: format.age
        });

        ageSlider.noUiSlider.on('change', _this.search.bind(_this));
    }

    Plugin.prototype.formatDate = function (date) {

        // Isolate the day, the month and the year from the date
        var dd = date.getDate();
        var mm = date.getMonth() + 1; // January is 0!
        var yy = date.getFullYear();

        //combine the date elements as mm/dd/yy
        //start by creating a leading zero for single digit days and months
        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }

        return dd + '/' + mm + '/' + yy; // 'DD/MM/YYYY'
    }

    Plugin.prototype.renderTemplate = function (stallion) {

        var html = _this.template;

        // fill data
        html = html.replace(/{ID}/g, stallion._id);
        html = html.replace(/{REVIEWS}/g, stallion.reviews.length);
        html = html.replace(/{COMMENTS}/g, stallion.commentaar.length);
        html = html.replace(/{BIRTHNAME}/g, stallion.birthname);
        html = html.replace(/{SIRE}/g, stallion.sire);
        html = html.replace(/{FILE}/g, stallion.file[0]);
        html = html.replace(/{SIREDAM}/g, stallion.siredam);
        html = html.replace(/{HEIGHT}/g, stallion.height);
        html = html.replace(/{COLOR}/g, stallion.color);
        html = html.replace(/{LEVEL}/g, Number(stallion.level).toFixed(2));
        html = html.replace(/{STUDBOOK}/g, stallion.studbook);
        html = html.replace(/{APPROVED}/g, stallion.approved.join(' '));
        html = html.replace(/{STUDFEE}/g, stallion.studfee);
        html = html.replace(/{SEMEN}/g, stallion.semen.join(', '));
        html = html.replace(/{SLUG}/g, stallion.slug);
        html = html.replace(/{ORDERSEMEN}/g, stallion.ordersemen);

        var dob = (stallion.dateOfBirth || '').split('T')[0]; // get "2004-04-02" from "2004-04-02T00:00:00.000Z"
        html = html.replace(/{DOB}/g, _this.formatDate(new Date(dob)));

        return html;
    }

    Plugin.prototype.renderList = function (data) {

        // clear container
        _this.$container.html('');

        if (Array.isArray(data.stallions) && data.stallions.length > 0) {
            const list = data.stallions.map(this.renderTemplate.bind(this));
            _this.$container.html(list);
        }
        else {
            _this.$container.html('<h4>Sorry, no matching results found.</h4>');
        }

        this.renderPagination({
            page: data.page,
            pages: data.pages,
            count: data.count
        });
    }

    Plugin.prototype.renderPagination = function (data) {
        var $prev = _this.$pagination.find('.pagination__prev a').hide();
        var $next = _this.$pagination.find('.pagination__next a').hide();
        var $text = _this.$pagination.find('.pagination__text p');

        var page = parseInt(data.page, 10);
        var count = parseInt(data.count, 10);
        var pages = parseInt(data.pages, 10);

        if (page > 1) {
            $prev.attr('href', '/stallions/page/' + (page - 1)).show();
        }

        if (page < pages) {
            $next.attr('href', '/stallions/page/' + (page + 1)).show();
        }
        
        $text.text('Page ' + page + ' of ' + pages);
    }

    Plugin.prototype.search = function () {

        var data = this.collectData();

        // send search query
        $.ajax({
            url: '/stallions/search',
            method: 'GET',
            data: data,
            success: this.renderList.bind(this)
        }).fail(function (xhr, textStatus) {
            _this.$container.html('<h4>Oops, something bad happened!</h4>');
        });
    }

    $.fn[pluginName] = function (options) {
        return this.each(function () {
            $.data(this, pluginName, new Plugin(this, options));
        });
    };
})(jQuery, wNumb, window, document);
